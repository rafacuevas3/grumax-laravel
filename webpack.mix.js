let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
   
   // index modules
   .js('resources/assets/js/modules/proveedores/index.js', 'public/js/index.proveedores.js')
   .js('resources/assets/js/modules/clientes/index.js', 'public/js/index.clientes.js')
   .js('resources/assets/js/modules/unidades/index.js', 'public/js/index.unidades.js')
   .js('resources/assets/js/modules/monedas/index.js', 'public/js/index.monedas.js')
   .js('resources/assets/js/modules/categorias/index.js', 'public/js/index.categorias.js')
   .js('resources/assets/js/modules/almacenes/index.js', 'public/js/index.almacenes.js')

   // Controller modules
   .js('resources/assets/js/modules/proveedores/proveedores.js', 'public/js')
   .js('resources/assets/js/modules/clientes/clientes.js', 'public/js')

   .js('resources/assets/js/app.js', 'public/js');

mix.js('resources/assets/js/modules/clasificaciones/index.js', 'public/js/index.clasificaciones.js');

mix.js('resources/assets/js/modules/tallas/index.js', 'public/js/index.tallas.js');
