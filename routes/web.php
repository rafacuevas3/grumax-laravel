<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/catalogos', function () { return view('catalogos'); })->name('catalogos');
    
    // Proveedores
    Route::get('proveedores', 'Proveedor\ListController@index')->name('proveedores.index');
    Route::get('proveedores/crear', 'Proveedor\UpdateController@create')->name('proveedores.create');
    Route::post('proveedores', 'Proveedor\UpdateController@store')->name('proveedores.store');
    Route::get('proveedores/{proveedor}', 'Proveedor\ListController@show')->name('proveedores.show');
    Route::get('proveedores/editar/{proveedor}', 'Proveedor\UpdateController@edit')->name('proveedores.edit');
    Route::put('proveedores/{proveedor}', 'Proveedor\UpdateController@update')->name('proveedores.update');
    Route::delete('proveedores/{proveedor}', 'Proveedor\UpdateController@destroy')->name('proveedores.destroy');

    // Clientes
    Route::get('clientes', 'Cliente\ListController@index')->name('clientes.index');
    Route::get('clientes/crear', 'Cliente\UpdateController@create')->name('clientes.create');
    Route::post('clientes', 'Cliente\UpdateController@store')->name('clientes.store');
    Route::get('clientes/{cliente}', 'Cliente\ListController@show')->name('clientes.show');
    Route::get('clientes/editar/{cliente}', 'Cliente\UpdateController@edit')->name('clientes.edit');
    Route::put('clientes/{cliente}', 'Cliente\UpdateController@update')->name('clientes.update');
    Route::delete('clientes/{cliente}', 'Cliente\UpdateController@destroy')->name('clientes.destroy');
    
    // Unidades
    Route::get('unidades', 'UnidadController@index')->name('unidades.index');
    Route::post('unidades/all', 'UnidadController@all')->name('unidades.all');
    Route::get('unidades/crear', 'UnidadController@create')->name('unidades.create');
    Route::post('unidades', 'UnidadController@store')->name('unidades.store');
    Route::get('unidades/{unidad}', 'UnidadController@show')->name('unidades.show');
    Route::get('unidades/editar/{unidad}', 'UnidadController@edit')->name('unidades.edit');
    Route::put('unidades/{unidad}', 'UnidadController@update')->name('unidades.update');
    Route::delete('unidades/{unidad}', 'UnidadController@destroy')->name('unidades.delete');
    
    // Categorias
    Route::get('categorias', 'CategoriaController@index')->name('categorias.index');
    Route::post('categorias/all', 'CategoriaController@all')->name('categorias.all');
    Route::get('categorias/crear', 'CategoriaController@create')->name('categorias.create');
    Route::post('categorias', 'CategoriaController@store')->name('categorias.store');
    Route::get('categorias/{unidad}', 'CategoriaController@show')->name('categorias.show');
    Route::get('categorias/editar/{unidad}', 'CategoriaController@edit')->name('categorias.edit');
    Route::put('categorias/{unidad}', 'CategoriaController@update')->name('categorias.update');
    Route::delete('categorias/{unidad}', 'CategoriaController@destroy')->name('categorias.delete');

    // Monedas
    Route::get('monedas', 'MonedaController@index')->name('monedas.index');
    Route::post('monedas/all', 'MonedaController@all')->name('monedas.all');
    Route::get('monedas/crear', 'MonedaController@create')->name('monedas.create');
    Route::post('monedas', 'MonedaController@store')->name('monedas.store');
    Route::get('monedas/{unidad}', 'MonedaController@show')->name('monedas.show');
    Route::get('monedas/editar/{unidad}', 'MonedaController@edit')->name('monedas.edit');
    Route::put('monedas/{unidad}', 'MonedaController@update')->name('monedas.update');
    Route::delete('monedas/{unidad}', 'MonedaController@destroy')->name('monedas.delete');

    // Almacenes
    Route::get('almacenes', 'AlmacenController@index')->name('almacenes.index');
    Route::post('almacenes/all', 'AlmacenController@all')->name('almacenes.all');
    Route::get('almacenes/crear', 'AlmacenController@create')->name('almacenes.create');
    Route::post('almacenes', 'AlmacenController@store')->name('almacenes.store');
    Route::get('almacenes/{unidad}', 'AlmacenController@show')->name('almacenes.show');
    Route::get('almacenes/editar/{unidad}', 'AlmacenController@edit')->name('almacenes.edit');
    Route::put('almacenes/{unidad}', 'AlmacenController@update')->name('almacenes.update');
    Route::delete('almacenes/{unidad}', 'AlmacenController@destroy')->name('almacenes.delete');

    // Clasificaciones
    Route::get('clasificaciones', 'ClasificacionController@index')->name('clasificaciones.index');
    Route::post('clasificaciones/all', 'ClasificacionController@all')->name('clasificaciones.all');
    Route::get('clasificaciones/crear', 'ClasificacionController@create')->name('clasificaciones.create');
    Route::post('clasificaciones', 'ClasificacionController@store')->name('clasificaciones.store');
    Route::get('clasificaciones/{unidad}', 'ClasificacionController@show')->name('clasificaciones.show');
    Route::get('clasificaciones/editar/{unidad}', 'ClasificacionController@edit')->name('clasificaciones.edit');
    Route::put('clasificaciones/{unidad}', 'ClasificacionController@update')->name('clasificaciones.update');
    Route::delete('clasificaciones/{unidad}', 'ClasificacionController@destroy')->name('clasificaciones.delete');

    // Tallas
    Route::get('tallas', 'TallaController@index')->name('tallas.index');
    Route::post('tallas/all', 'TallaController@all')->name('tallas.all');
    Route::get('tallas/crear', 'TallaController@create')->name('tallas.create');
    Route::post('tallas', 'TallaController@store')->name('tallas.store');
    Route::get('tallas/{unidad}', 'TallaController@show')->name('tallas.show');
    Route::get('tallas/editar/{unidad}', 'TallaController@edit')->name('tallas.edit');
    Route::put('tallas/{unidad}', 'TallaController@update')->name('tallas.update');
    Route::delete('tallas/{unidad}', 'TallaController@destroy')->name('tallas.delete');
});