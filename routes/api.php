<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Proveedores
Route::post('proveedores', 'Proveedor\apiController')->name('proveedores.get');
Route::post('proveedores/all', 'Proveedor\apiListController')->name('proveedores.all');

// Clientes
Route::post('clientes', 'Cliente\apiController')->name('clientes.get');
Route::post('clientes/all', 'Cliente\apiListController')->name('clientes.all');