Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'almacenes',
            url_key: 'id',
        },
        {
            title: 'Descripción',
            key: 'descripcion',
            url: 'almacenes',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'almacenes', key: 'id' },
        { icon: 'fa-pencil', url: 'almacenes/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/almacenes', key: 'id' },
        get: '/almacenes/all',
    }
  },
})