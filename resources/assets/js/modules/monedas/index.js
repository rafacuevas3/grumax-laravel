Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'monedas',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'monedas', key: 'id' },
        { icon: 'fa-pencil', url: 'monedas/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/monedas', key: 'id' },
        get: '/monedas/all',
    }
  },
})