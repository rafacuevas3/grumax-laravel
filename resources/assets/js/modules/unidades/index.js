Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'unidades',
            url_key: 'id',
        },
        {
            title: 'Abreviatura',
            key: 'abreviatura',
            url: 'unidades',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'unidades', key: 'id' },
        { icon: 'fa-pencil', url: 'unidades/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/unidades', key: 'id' },
        get: '/unidades/all',
    }
  },
})