Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        // Module columns for table
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'tallas',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'tallas', key: 'id' },
        { icon: 'fa-pencil', url: 'tallas/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/tallas', key: 'id' },
        get: '/tallas/all',
    }
  },
})