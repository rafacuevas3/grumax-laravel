Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'categorias',
            url_key: 'id',
        },
        {
            title: 'Descripcion',
            key: 'descripcion',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'categorias', key: 'id' },
        { icon: 'fa-pencil', url: 'categorias/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/categorias', key: 'id' },
        get: '/categorias/all',
    }
  }
})