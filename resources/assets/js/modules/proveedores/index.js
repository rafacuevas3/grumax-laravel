Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre_oficial',
            url: 'proveedores',
            url_key: 'id',
        },
        {
            title: 'Nombre corto',
            key: 'nombre',
            url: 'proveedores',
            url_key: 'id',
        },
        {
            title: 'RFC',
            key: 'rfc',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'proveedores', key: 'id' },
        { icon: 'fa-pencil', url: 'proveedores/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/proveedores', key: 'id' },
        get: '/api/proveedores/all',
    }
  },
})