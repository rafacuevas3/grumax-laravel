Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        // Module columns for table
        {
            title: 'Nombre',
            key: 'nombre',
            url: 'clasificaciones',
            url_key: 'id',
        },
        {
            title: 'Descripción',
            key: 'descripcion',
            url: 'clasificaciones',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'clasificaciones', key: 'id' },
        { icon: 'fa-pencil', url: 'clasificaciones/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/clasificaciones', key: 'id' },
        get: '/clasificaciones/all',
    }
  },
})