Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        {
            title: 'Nombre',
            key: 'nombre_oficial',
            url: 'clientes',
            url_key: 'id',
        },
        {
            title: 'Nombre corto',
            key: 'nombre',
            url: 'clientes',
            url_key: 'id',
        },
        {
            title: 'RFC',
            key: 'rfc',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: 'clientes', key: 'id' },
        { icon: 'fa-pencil', url: 'clientes/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/clientes', key: 'id' },
        get: '/api/clientes/all',
    }
  },
})