Vue.filter('capitalize', function (str) { return str[0].toUpperCase() + str.slice(1); });

Vue.filter('max', function (str, max) { return str.substr(0, max).trim(); });

Vue.filter('parseBool', function (val) { return val ? 1 : 0; });

Vue.component('direccion', require('../../components/Direccion.vue'));

Vue.component('contacto', require('../../components/Contacto.vue'));

import steps from '../../mixins/steps.js';

const app = new Vue({
    el: '#app',
    mixins: [steps],
    data: {
        cliente: {
            nombre: "",
            nombre_factura: "",
            nombre_oficial: "",

            comentario: "",
            
            direcciones: [{pais: 'Mexico'}],
            contactos: [{telefonos: [{tipo: 'oficina'}], correos: [{}]}],
            
            direccion_factura: {},
            
            direcciones_recoleccion: [
                {}
            ],

            isEmbarque: 0,
            rfc: "",
            tipo: "Nacional",
        }
    },
    methods: {
        addDireccion: function () {
            this.cliente.direcciones.push({pais: 'Mexico'});
        },
        addContacto: function () {
            this.cliente.contactos.push({telefonos: [{tipo: 'oficina'}], correos: [{}]});
        },
        removeItem: function (index, prop) {
            this.cliente[prop].splice(index, 1);
        },

        initData: function (id) {
            axios.post('/api/clientes', { id: id })
                 .then(function (response) {
                     app.cliente = response.data.cliente;
                 })
                 .catch(function (error) {
                     console.log(error.response);
                 });
        }
    },
    mounted: function () {
        var url = window.location.href.split("/");

        if (url.indexOf('editar') !== -1) {
            this.initData(url[url.length - 1]);
        }
    }
});
