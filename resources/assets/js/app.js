
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(function() {
    var w = (100 / $('.stepwizard-step').length);
    
    $('.step-progress').width((w/2) + '%');

    $('#button-logout').on('click', function () {
        swal({
            title: '¿Deseas salir del sistema?',
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Salir'
        }).then(function () {
            axios.post('/logout')
                .then(function (response) {
                    swal('Gracias por usar el sistema!', '', 'success');

                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        })
    });
});