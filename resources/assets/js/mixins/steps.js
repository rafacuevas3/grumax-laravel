export default {
    data: {
        state: 0,
        titles: [{}, {}, {}],
    },
    methods: {
        setState: function (val) {
            this.state = val;
            var w = (100 / this.titles.length);
            var index = this.state;
            $('.step-progress').width((w * (index)) + (w/2) + '%');
        },
        moveState: function (val) {
            this.state += val;
            
            var w = (100 / this.titles.length);
            
            var index = this.state;
            
            $('.step-progress').width((w * (index)) + (w/2) + '%');
        }
    }
};