@extends(Auth::user()->template ?? 'layouts.app')

@section('title', 'Catálogos | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Catálogos</h3>
    
    <div class="row">
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('almacenes.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Almacenes</h5>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('categorias.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Categorias</h5>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('clasificaciones.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Clasificaciones</h5>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('monedas.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Monedas</h5>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('tallas.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Tallas</h5>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 text-center">
            <a href="{{ route('unidades.index') }}" class="tag-menu-link">
                <img class="img-responsive center-block" src="{{ asset('img/categorias_128.png') }}">
                <h5>Unidades</h5>
            </a>
        </div>
    </div>
    @endsection