<header class="main-header">
    <nav class="navbar navbar-static-top" role="navigation">
        @if(isset($last_url))
            <a class="navigation-item" href="{{ url($last_url) }}"><i class="fa fa-arrow-left"></i></a>
        @endif

        <!-- Logo Grumax -->
        <div class="navbar-header">
            <a href="{{ url('inicio') }}" class="logo" id="btn-toggle-spam">
                <span class="logo-mini"><i class="fa fa-home"></i></span>
                <span class="logo-lg"><b>Grumax</b></span>
            </a>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-cog"></i>
            </button>
        </div>

        <!-- Menú de navegación -->
        <div class="navbar-custom-menu pull-left">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle item-primary" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa icon-items"></i><span class="hide-on-md">Materiales</span><span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('proveedores') }}"><i class="fa fa-building"></i>&nbsp; <span>Proveedores</span></a></li>
                            <li class="divider"></li>
                        
                        <li><a href="{{ url('articulos') }}"><i class="fa icon-items"></i>&nbsp; <span>Artículos</span></a></li>
                        

                        <li><a href="{{ url('compras') }}"><i class="fa icon-coins"></i>&nbsp; <span>Compras</span></a></li>
                        

                        <li><a href="{{ url('envios') }}"><i class="fa fa-plane"></i>&nbsp; <span>Envios</span></a></li>
                        
                            
                        <li><a href="{{ url('entradas') }}"><i class="fa icon-in"></i>&nbsp; <span>Entradas</span></a></li>
                        
                            
                        <li><a href="{{ url('salidas') }}"><i class="fa icon-out"></i>&nbsp; <span>Salidas</span></a></li>
                        

                        <li><a href="{{ url('auditorias') }}"><i class="fa fa-edit"></i>&nbsp; <span>Auditorias</span></a></li>
                        
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle item-primary" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-industry"></i><span class="hide-on-md">Producción</span><span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('clientes') }}"><i class="fa icon-clients"></i>&nbsp; <span>Clientes</span></a></li>

                        <li><a href="{{ url('estilos') }}"><i class="fa icon-shirt"></i>&nbsp; <span>Estilos</span></a></li>

                        <li><a href="{{ url('muestras') }}"><i class="fa icon-shirt"></i>&nbsp; <span>Muestras</span></a></li>

                        <li class="divider"></li>

                        <li><a href="{{ url('mix') }}"><i class="fa fa-percent"></i>&nbsp; <span>Mix porcentaje</span></a></li>

                        <li><a href="{{ url('mix_talla') }}"><i class="fa fa-text-height"></i>&nbsp; <span>Mix talla</span></a></li>
                    

                        <li><a href="{{ url('pedidos') }}"><i class="fa fa-shopping-cart"></i>&nbsp; <span>Pedidos</span></a></li>

                        <li class="dropdown-submenu">
                            <a href="{{ url('produccion') }}" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa icon-production"></i>&nbsp; <span>Ordenes de producción</span> &nbsp;&nbsp;
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('produccion/crear') }}"><i class="fa fa-plus"></i>&nbsp; Nuevo</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ url('cortes') }}">
                                <i class="fa fa-cut"></i>&nbsp; <span>Corte</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url('avance_produccion') }}">
                                <i class="fa fa-line-chart"></i>&nbsp; <span>Avance de producción</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url('terminado') }}">
                                <i class="fa fa-check-square"></i>&nbsp; <span>Terminado</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url('embarques') }}">
                                <i class="fa fa-truck"></i>&nbsp; <span>Embarques</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ url('wip') }}">
                                <i class="fa fa-bar-chart"></i>&nbsp; <span>Wip</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle item-primary" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-cogs"></i><span class="hide-on-md">Administración</span><span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('catalogos') }}"><i class="fa fa-gears"></i>&nbsp; <span>Catálogos</span></a></li>

                        <li><a href="{{ url('bitacora') }}"><i class="fa fa-calendar"></i>&nbsp; <span>Bitácora</span></a></li>

                        <li><a href="#"><i class="fa fa-users"></i>&nbsp; <span>Usuarios</span></a></li>

                        <li><a href="{{ url('fastreact') }}"><i class="fa icon-fastreact"></i>&nbsp; <span>Fast React</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <!-- Panel derecho -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @yield('tools')

                <!-- Usuarios conectados -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-users"></i>
                        <span class="label label-success">0</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">No hay usuarios conectados</li>
                    </ul>
                </li>
                
                <!-- User profile -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('img/user_logo_3.png') }}" alt="No hay imagen" class="user-image">
                        <span class="hidden-xs bold">{{ Auth::user()->username }}</span>
                        &nbsp;
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ url('img/user_logo_3.png') }}" alt="No hay imagen" class="img-circle">
                            <p>
                                {{ Auth::user()->username }}
                                <small>{{ Auth::user()->name }}</small>
                                <small>{{ Auth::user()->email }}</small>
                            </p>
                        </li>
                        <!-- <li class="user-body"></li> -->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('account_settings/config') }}" class="btn btn-default btn-flat"><i class="fa fa-user"></i>&nbsp; Perfil</a>
                            </div>
                            <div class="pull-right">
                                <button id="button-logout" type="button" class="btn btn-danger btn-flat"><i class="fa fa-power-off"></i>&nbsp; <b>Salir</b></button>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>