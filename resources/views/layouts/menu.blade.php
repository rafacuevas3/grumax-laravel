<li>
    <a href="{{ route('proveedores.index') }}"><i class="fa fa-industry"></i><span>Proveedores</span></a>
</li><li class="{{ Request::is('unidads*') ? 'active' : '' }}">
    <a href="{!! route('unidads.index') !!}"><i class="fa fa-edit"></i><span>Unidads</span></a>
</li>

<li class="{{ Request::is('categorias*') ? 'active' : '' }}">
    <a href="{!! route('categorias.index') !!}"><i class="fa fa-edit"></i><span>Categorias</span></a>
</li>

<li class="{{ Request::is('monedas*') ? 'active' : '' }}">
    <a href="{!! route('monedas.index') !!}"><i class="fa fa-edit"></i><span>Monedas</span></a>
</li>

<li class="{{ Request::is('almacenes*') ? 'active' : '' }}">
    <a href="{!! route('almacenes.index') !!}"><i class="fa fa-edit"></i><span>Almacenes</span></a>
</li>

<li class="{{ Request::is('clasificacions*') ? 'active' : '' }}">
    <a href="{!! route('clasificacions.index') !!}"><i class="fa fa-edit"></i><span>Clasificacions</span></a>
</li>

<li class="{{ Request::is('tallas*') ? 'active' : '' }}">
    <a href="{!! route('tallas.index') !!}"><i class="fa fa-edit"></i><span>Tallas</span></a>
</li>

