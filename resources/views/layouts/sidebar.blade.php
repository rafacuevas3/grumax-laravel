<aside class="main-sidebar" id="sidebar-wrapper">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name}}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" id="sidebar-menu-navigation">
            @include('layouts.menu')
        </ul>
    </section>
</aside>