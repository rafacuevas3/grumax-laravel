<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        @yield('title') Grumax ERP
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @yield('css')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div id="app">
        <div class="wrapper">
            @include('layouts.sidebar')

            @include('layouts.navbar')
            
            <div class="content-wrapper">
                <section class="content" style="padding-bottom: 0;">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            @section('container')
                                <div class="box @yield('box-class') flat">
                                    <div class="overlay hide">
                                        <i class="fa fa-refresh fa-spin"></i>
                                    </div>
                                    <div class="box-body">@yield('content')</div>
                                </div>
                            @show
                        </div>
                    </div>
                </section>
            </div>

            @include('layouts.footer')
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')
</body>
</html>