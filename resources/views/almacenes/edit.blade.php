@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('almacenes.index'))

@section('title', 'Edición de Almacen | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Almacen</h3>
    
    {!! Form::model($almacen, [
        'route' => ['almacenes.update', $almacen->id],
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        'method' => 'PUT']) !!}
        
        @include('almacenes.fields')
    {!! Form::close() !!}
@endsection