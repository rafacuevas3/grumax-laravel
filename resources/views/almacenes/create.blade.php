@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('almacenes.index'))

@section('title', 'Registro de Almacen | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Almacen</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('almacenes.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('almacenes.fields')
    </form>
@endsection