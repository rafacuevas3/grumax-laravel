<!-- Nombre Field -->

<tr>
    <td class="text-bold">
        Nombre
    </td>
    <td>
        {!! $almacen->nombre !!}
    </td>
</tr>

<!-- Descripcion Field -->

<tr>
    <td class="text-bold">
        Descripcion
    </td>
    <td>
        {!! $almacen->descripcion !!}
    </td>
</tr>