<!-- Nombre Field -->
<tr>
    <td class="text-bold">
        Nombre:
    </td>
    <td>
        {!! $categoria->nombre !!}
    </td>
</tr>

<!-- Descripcion Field -->
<tr>
    <td class="text-bold">
        Descripción:
    </td>
    <td>
        {!! $categoria->descripcion !!}
    </td>
</tr>