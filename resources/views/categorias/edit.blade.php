@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('categorias.index'))

@section('title', 'Edición de Categoria | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Categoria</h3>
    
    {!! Form::model($categoria, [
        'route' => ['categorias.update', $categoria->id],
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        'method' => 'PUT']) !!}
        
        @include('categorias.fields')
    {!! Form::close() !!}
@endsection