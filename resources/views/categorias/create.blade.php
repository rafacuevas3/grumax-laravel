@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('categorias.index'))

@section('title', 'Registro de Categoria | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Categoria</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('categorias.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('categorias.fields')
    </form>
@endsection