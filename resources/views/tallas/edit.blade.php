@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('tallas.index'))

@section('title', 'Edición de Talla | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Talla</h3>
    
    {!! Form::model($talla, [
        'route' => ['tallas.update', $talla->id],
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        'method' => 'PUT']) !!}
        
        @include('tallas.fields')
    {!! Form::close() !!}
@endsection