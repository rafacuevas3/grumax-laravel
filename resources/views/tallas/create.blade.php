@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('tallas.index'))

@section('title', 'Registro de Talla | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Talla</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('tallas.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('tallas.fields')
    </form>
@endsection