@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('clientes.index'))

@section('title', 'Registro de cliente | ')
@section('box-class', 'box-primary')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/slider-steps.css') }}">
    <link rel="stylesheet" href="{{ asset('css/simple-checkbox/simple-checkbox.css') }}">

    <style>
        .box-close-button { position: absolute; right: 15px; top: 7px; }
        .panel-heading-hover:hover {
            background: #FAFAFA;
            cursor: pointer;
        }
        .panel-heading-hover:active {
            background: #F3F3F3;
        }
    </style>
@endsection

@section('tools')
    <li class="dropdown">
        <a href="#" class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i>&nbsp; Herramientas
        </a>

        <ul class="dropdown-menu" role="menu">
            <li><a @click.prevent="addContacto"><i class="fa fa-user"></i>&nbsp; Agregar contacto</a></li>
            <li><a @click.prevent="addDireccion"><i class="fa fa-list"></i>&nbsp; Agregar dirección</a></li>
        </ul>
    </li>
@endsection

@section('content')
    <h3 class="text-center">Registrar cliente</h3>
    
    <form class="form-horizontal" method="post" action="{{ route('clientes.store') }}" autocomplete="off">
        {{ csrf_field() }}
        
        @include('clientes.partials.fields')
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/clientes.js') }}" type="text/javascript"></script>
@endsection