@extends(Auth::user()->template ?? 'layouts.app')

@section('title', 'Clientes | ')
@section('box-class', 'box-danger')

@section('tools')
    <li class="dropdown">
        <a class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i><span class="hide-on-md">Herramientas</span><span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li><a class="item-success" href="{{ url('clientes/crear') }}"><i class="fa fa-plus"></i>&nbsp; Nuevo</a></li>
            <li class="divider"></li>
            
            <li>
                <a target="_blank" class="item-success" id="btn-pdf" href="{{ url('clientes/pdf') }}">
                    <i class="fa fa-file-pdf-o"></i>&nbsp; PDF
                </a>
            </li>
        </ul>
    </li>
@endsection

@section('opciones')
    <li class="dropdown">
        <a class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i><span class="hide-on-md">Herramientas</span><span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
        
            @can('clientes_escritura')
                <li><a class="item-success" href="proveedor/crear"><i class="fa fa-plus"></i>&nbsp; Nuevo</a></li>
                <li class="divider"></li>
            @endcan
        
            
            <li><a target="_blank" class="item-success" href="proveedor/pdf"><i class="fa fa-file-pdf-o"></i>&nbsp; PDF</a></li>
            <li><a class="item-success" id="btn-excel" href="#"><i class="fa fa-file-excel-o"></i>&nbsp; Excel</a></li>
        </ul>
    </li>
@endsection

@section('content')
    <h3><i class="fa icon-clients"></i>&nbsp; Clientes</h3>
    
    <grid :columns="gridColumns" :buttons="buttons" :routes="routes"></grid>
@endsection

@section('scripts')
    <script src="/js/index.clientes.js" type="text/javascript"></script>
@endsection