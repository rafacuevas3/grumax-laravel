<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="step-progress"></div>
        <div class="stepwizard-step" :class="{on: state == 0, success: state > 0}" :style="{width: 100/titles.length + '%'}">
            <button type="button" @click="setState(0)"></button>
            <p>Datos generales</p>
        </div>
        <div class="stepwizard-step" :class="{on: state == 1, success: state > 1}" :style="{width: 100/titles.length + '%'}">
            <button type="button" @click="setState(1)"></button>
            <p>Datos de recolección</p>
        </div>
        <div class="stepwizard-step" :class="{on: state == 2, success: state > 2}" :style="{width: 100/titles.length + '%'}">
            <button type="button" @click="setState(2)"></button>
            <p>Contacto(s)</p>
        </div>
    </div>
</div>

<div>
    @include('clientes.tabs.datos_generales')
    @include('clientes.tabs.datos_recoleccion')
    @include('clientes.tabs.contacto')
</div>

<button type="button" class="float-button left" v-show="state > 0" @click="moveState(-1)"><i class="fa fa-chevron-left"></i></button>
<button type="button" class="float-button right" v-show="state < 2" @click="moveState(1)"><i class="fa fa-chevron-right"></i></button>

<!-- Next and prev buttons -->
<div class="row">
    <div class="col-xs-12 text-center">
        <input type="submit" class="btn btn-success" value="Guardar" />
        <a class="btn btn-danger" href="{{ route('clientes.index') }}">Cancelar</a>
    </div>
</div>