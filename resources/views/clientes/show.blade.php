@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('clientes.index'))

@section('title', 'Cliente '. $cliente->nombre .' | ')
@section('box-class', 'box-primary')

@section('tools')
    <li class="dropdown">
        <a class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i><span class="hide-on-md">Herramientas</span><span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{ route('clientes.create') }}">
                    <i class="fa fa-plus"></i>&nbsp; Nuevo
                </a>
            </li>
            <li>
                <a href="{{ route('clientes.edit', $cliente->id) }}">
                    <i class="fa fa-pencil"></i>&nbsp; Editar
                </a>
            </li>
        </ul>
    </li>
@endsection

@section('container')
    <div class="box box-primary border-flat">
        <div class="box-header">
            <h3 class="text-center">DATOS DEL CLIENTE</h3>
        </div>
        <div class="box-body no-padding">
            <div class="nav-tabs-custom no-margin">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Datos generales</a></li>
                    <li role="presentation"><a href="#embarque" aria-controls="embarque" role="tab" data-toggle="tab">Datos de recolección</a></li>
                    <li role="presentation"><a href="#contacto" aria-controls="contacto" role="tab" data-toggle="tab">Contactos</a></li>
                    <li role="presentation"><a href="#articulos" aria-controls="articulos" role="tab" data-toggle="tab">Articulos</a></li>
                </ul>
                <div class="tab-content no-padding">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <br>
                        <div class="row" style="padding-left: 10px;">
                            <div class="col-xs-7 col-md-8">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nombre</label>
                                        <div class="col-md-8">{{ $cliente->nombre_factura }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nombre corto</label>
                                        <div class="col-md-8">{{ $cliente->nombre }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">RFC</label>
                                        <div class="col-md-8">{{ $cliente->rfc }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Tipo</label>
                                        <div class="col-md-8">{{ $cliente->tipo }}</div>
                                    </div>
                                </div>

                                {{-- terminos.tipo == 'credito' --}}
                                    
                                    {{-- <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Limite de credito</label>
                                            <div class="col-md-8">
                                                $ {{ terminos.limite_credito|round_format }}
                                                <small>{{ moneda.nombre == 1 ? 'USD' : 'MXN' }}</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Dias naturales</label>
                                            <div class="col-md-8">{{ terminos.dias_naturales }}</div>
                                        </div>
                                    </div> --}}

                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Comentarios</label>
                                        <div class="col-md-8">{{ $cliente->comentario }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-8"><h4>Dirección del cliente</h4></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Calle</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->calle }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Colonia</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->colonia }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Numero</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->numero }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">C.P</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->cp }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Municipio</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->municipio }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Estado</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->estado }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">País</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->pais }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Teléfono</label>
                                        <div class="col-md-8">{{ $cliente->direccion_factura()->telefono }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="embarque">
                        <div class="row" style="padding-left: 10px;">
                            <div class="col-md-10">
                                <br>
                                <div class="form-group">
                                    <h4>Direcciones de recolección</h4>
                                </div>
                                <div class="form-group">
                                    @if ($cliente->direcciones_recoleccion()->count() > 0)
                                        <div class="panel-group" id="accordion_embarque" role="tablist" aria-multiselectable="true">
                                            @foreach ($cliente->direcciones_recoleccion() as $direccion)
                                                <div class="panel panel-default flat">
                                                    <div class="panel-heading" role="tab" id="heading{{ $loop->index }}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $loop->index }}" aria-expanded="true" aria-controls="collapse{{ $loop->index }}">
                                                        <h6 class="panel-title">Dirección: {{ $direccion->nombre ?: $direccion->calle }}</a>
                                                        </h6>
                                                    </div>
                                                    <div id="collapse{{ $loop->index }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $loop->index }}">
                                                        <div class="panel-body">
                                                            <table class="table">
                                                                <tr>
                                                                    <td> <b>Nombre</b> </td>
                                                                    <td> {{ $direccion->nombre }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Comentario</b> </td>
                                                                    <td> {{ $direccion->comentario }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Teléfono</b> </td>
                                                                    <td> {{ $direccion->telefono }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Calle</b> </td>
                                                                    <td> {{ $direccion->calle }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Colonia</b> </td>
                                                                    <td> {{ $direccion->colonia }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Número de calle</b> </td>
                                                                    <td> {{ $direccion->numero }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Código postal</b> </td>
                                                                    <td> {{ $direccion->cp }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Municipio</b> </td>
                                                                    <td> {{ $direccion->municipio }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>Estado</b> </td>
                                                                    <td> {{ $direccion->estado }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <b>País</b> </td>
                                                                    <td> {{ $direccion->pais }} </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @elseif ($cliente->isEmbarque)
                                        <h4 class="text-danger">Las direcciones de recolección esta pendientes por registrarse</h4>
                                    @else
                                        <h4 class="text-danger">Este cliente no tiene direcciones</h4>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="contacto">
                        <div class="row" style="padding-left: 10px;">
                            <div class="col-xs-7 col-md-8">
                            <br>
                                @forelse ($cliente->contactos as $index => $contacto)
                                    <div class="row @if ($index > 1) top-line @endif">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nombre</label>
                                            <div class="col-md-8">{{ $contacto->nombre }}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Departamento</label>
                                            <div class="col-md-8">{{ $contacto->departamento }}</div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Telefonos</label>
                                            <div class="col-md-8">
                                                @foreach ($contacto->telefonos as $telefono)
                                                    <div @if ($loop->index > 1) style="padding-top: 5px;" @endif>
                                                        <b>{{ $telefono->tipo }}</b>: {{ $telefono->telefono }}<br>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Correos</label>
                                            <div class="col-md-8">
                                                @foreach ($contacto->correos as $correo)
                                                    <div @if ($loop->index > 1) style="padding-top: 5px;" @endif>
                                                        {{ $correo->correo }}<br>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h4 class="text-danger">No se registraron contactos</h4>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="articulos">
                            {{-- <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Clave Interna</th>
                                        <th>Clave Externa</th>
                                        <th>Color</th>
                                        <th>Unidad Interna</th>
                                        <th>Unidad Externa</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {% for articulo in articulos %}
                                        <tr>
                                            <td><a href="{{ base_url }}articulo/ver/{{ articulo.id }}/{{ articulo.id_color }}">{{ articulo.nombreInterno }}</a></td>
                                            <td>{{ articulo.nombreExterno }}</td>
                                            <td>{{ articulo.nombre_color }}</td>
                                            <td>{{ articulo.unidad_interna }}</td>
                                            <td>{{ articulo.unidad_externa }}</td>
                                            <td style="max-width: 300px;">{{ articulo.descripcion }}</td>
                                        </tr>
                                    {% endfor %}
                                </tbody>
                            </table>
                            
                            <h4 class="text-danger">No se registraron articulos</h4> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection