<div class="animated" v-show="state == 0">
    <div class="form-group">
        <label for="nombre_cliente" class="col-md-2 control-label">Nombre</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="nombre_cliente" id="nombre_cliente" required="required" 
                   v-model="cliente.nombre_factura"/>
        </div>
    </div>
    <div class="form-group">
        <label for="nombrecorto" class="col-md-2 control-label">Nombre corto</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="nombrecorto" id="nombrecorto" required="required" 
                   v-model="cliente.nombre"/>
        </div>
    </div>
    <div class="form-group">
        <label for="rfc" class="col-md-2 control-label">RFC</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="rfc" id="rfc" required="required"
                   v-model="cliente.rfc"/>
        </div>
    </div>
    <div class="form-group">
        <label for="tipo" class="col-md-2 control-label">Tipo de cliente</label>
        <div class="col-md-8">
            <select name="tipo" id="tipo" class="form-control" v-model="cliente.tipo">
                <option value="Nacional">Nacional</option>
                <option value="Internacional">Internacional</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="comentario_cliente" class="col-md-2 control-label">Comentario</label>
        <div class="col-md-8">
            <textarea name="comentario_cliente" class="form-control" v-model="cliente.comentario"></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-8">
            <h4>Dirección del cliente</h4>
        </div>
    </div>

    <input type="hidden" name="id_direccion_recoleccion" v-model="cliente.direccion_factura.id">
    
    <div class="form-group">
        <label for="calle" class="col-md-2 control-label">Calle</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="calle" v-model="cliente.direccion_factura.calle"/>
        </div>
    </div>
    <div class="form-group">
        <label for="colonia" class="col-md-2 control-label">Colonia</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="colonia" v-model="cliente.direccion_factura.colonia"/>
        </div>
    </div>
    <div class="form-group">
        <label for="numero" class="col-md-2 control-label">Número</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="numero" v-model="cliente.direccion_factura.numero"/>
        </div>
        <label for="cp" class="col-md-2 control-label">C.P.</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="cp" v-model="cliente.direccion_factura.cp"/>
        </div>
    </div>
    <div class="form-group">
        <label for="municipio" class="col-md-2 control-label">Municipio</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="municipio" v-model="cliente.direccion_factura.municipio"/>
        </div>
    </div>
    <div class="form-group">
        <label for="estado" class="col-md-2 control-label">Estado</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="estado" v-model="cliente.direccion_factura.estado"/>
        </div>
    </div>
    <div class="form-group">
        <label for="pais" class="col-md-2 control-label">País</label>
        <div class="col-md-8">
            <select class="form-control" name="pais" v-model="cliente.direccion_factura.pais">
                <option value="" disabled selected>Seleccione un país</option>
                {{-- {% set cont = loop.index0 %}
                {% for pais in paises %}
                    <option value="{{ pais.Name }}" {% if direccion_cliente.pais == pais.Name %}selected{% endif %}>{{ pais.Name }}</option>
                {% endfor %} --}}
            </select>
        </div>
    </div>
</div>