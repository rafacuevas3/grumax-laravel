@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('monedas.index'))

@section('title', 'Edición de Moneda | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Moneda</h3>
    
    {!! Form::model($moneda, [
        'route' => ['monedas.update', $moneda->id],
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        'method' => 'PUT']) !!}
        
        @include('monedas.fields')
    {!! Form::close() !!}
@endsection