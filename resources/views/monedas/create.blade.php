@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('monedas.index'))

@section('title', 'Registro de Moneda | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Moneda</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('monedas.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('monedas.fields')
    </form>
@endsection