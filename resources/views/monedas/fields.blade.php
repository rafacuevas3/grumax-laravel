<!-- Nombre Field -->

<div class="row form-group">
    {!! Form::label('nombre', 'Nombre:', ['class' => 'col-md-2 text-right']) !!}
    
    <div class="col-md-8">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('monedas.index') !!}" class="btn btn-default">Cancelar</a>
</div>