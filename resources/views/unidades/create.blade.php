@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('unidades.index'))

@section('title', 'Registro de Unidad | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Unidad</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('unidades.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('unidades.fields')
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/unidades.js') }}" type="text/javascript"></script>
@endsection