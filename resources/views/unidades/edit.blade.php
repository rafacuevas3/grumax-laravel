@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('unidades.index'))

@section('title', 'Edición de Unidad | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Unidad</h3>
    
    {!! Form::model($unidad, [
        'route' => ['unidades.update', 
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        $unidad->id], 'method' => 'PUT']) !!}
        
        @include('unidades.fields')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script src="{{ asset('js/unidades.js') }}" type="text/javascript"></script>
@endsection