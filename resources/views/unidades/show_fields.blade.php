<!-- Abreviatura Field -->
<tr>
    <td class="text-bold">
        Abreviatura
    </td>
    <td>
        {!! $unidad->abreviatura !!}
    </td>
</tr>

<!-- Nombre Field -->
<tr>
    <td class="text-bold">
        Nombre
    </td>
    <td>
        {!! $unidad->nombre !!}
    </td>
</tr>