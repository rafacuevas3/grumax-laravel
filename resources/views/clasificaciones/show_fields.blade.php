<!-- Nombre Field -->

<tr>
    <td class="text-bold">
        Nombre
    </td>
    <td>
        {!! $clasificacion->nombre !!}
    </td>
</tr>

<!-- Descripcion Field -->

<tr>
    <td class="text-bold">
        Descripcion
    </td>
    <td>
        {!! $clasificacion->descripcion !!}
    </td>
</tr>