@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('clasificaciones.index'))

@section('title', 'Edición de Clasificacion | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Edición Clasificacion</h3>
    
    {!! Form::model($clasificacion, [
        'route' => ['clasificaciones.update', $clasificacion->id],
        'autocomplete' => 'off',
        'class' => 'form-horizontal',
        'method' => 'PUT']) !!}
        
        @include('clasificaciones.fields')
    {!! Form::close() !!}
@endsection