<table class="table table-responsive" id="clasificaciones-table">
    <thead>
        <tr>
            <th>Nombre</th>
        <th>Descripcion</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($clasificaciones as $clasificacion)
        <tr>
            <td>{!! $clasificacion->nombre !!}</td>
            <td>{!! $clasificacion->descripcion !!}</td>
            <td>
                {!! Form::open(['route' => ['clasificaciones.destroy', $clasificacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('clasificaciones.show', [$clasificacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('clasificaciones.edit', [$clasificacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>