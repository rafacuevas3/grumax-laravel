@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('clasificaciones.index'))

@section('title', 'Clasificacion | ')
@section('box-class', 'box-primary')

@section('tools')
    <li class="dropdown">
        <a class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i><span class="hide-on-md">Herramientas</span><span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{ route('clasificaciones.create') }}">
                    <i class="fa fa-plus"></i>&nbsp; Nuevo
                </a>
            </li>
            <li>
                <a href="{{ route('clasificaciones.edit', $clasificacion->id) }}">
                    <i class="fa fa-pencil"></i>&nbsp; Editar
                </a>
            </li>
        </ul>
    </li>
@endsection

@section('container')
    <div class="box box-primary border-flat">
        <div class="box-header">
            <h3 class="text-center uppercase">Clasificacion</h3>
        </div>
        <div class="box-body no-padding">
            <table class="table">
                <tbody>
                    @include('clasificaciones.show_fields')
                </tbody>
            </table>
        </div>
    </div>
@endsection
