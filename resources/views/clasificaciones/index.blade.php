@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('catalogos'))

@section('title', 'Clasificaciones | ')
@section('box-class', 'box-danger')

@section('tools')
    <li class="dropdown">
        <a class="item-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i><span class="hide-on-md">Herramientas</span><span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li><a class="item-success" href="{!! route('clasificaciones.create') !!}"><i class="fa fa-plus"></i>&nbsp; Nuevo</a></li>
        </ul>
    </li>
@endsection

@section('content')
    <h3>Clasificaciones</h3>
    
    <grid :columns="gridColumns" :buttons="buttons" :routes="routes"></grid>
@endsection

@section('scripts')
    <script src="/js/index.clasificaciones.js" type="text/javascript"></script>
@endsection