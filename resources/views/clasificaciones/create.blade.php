@extends(Auth::user()->template ?? 'layouts.app')

@php($last_url = route('clasificaciones.index'))

@section('title', 'Registro de Clasificacion | ')
@section('box-class', 'box-primary')

@section('content')
    <h3 class="text-center">Registrar Clasificacion</h3>
    
    <form class="form-horizontal" method="post" 
          action="{{ route('clasificaciones.store') }}" 
          autocomplete="off">
        
        {{ csrf_field() }}
        
        @include('clasificaciones.fields')
    </form>
@endsection