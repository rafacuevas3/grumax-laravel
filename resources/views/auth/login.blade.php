<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>Login | Grumax ERP</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
    <div class="container-fluid" id="app">
        <div class="row">
            <div class="col-xs-12">
                <div class="center-block centrado">
                    <form action="{{ url('login') }}" method="post" accept-charset="utf-8">
                        
                        {!! csrf_field() !!}
                        
                        <div class="form-group">
                            <img class="img-responsive center-block" src="{{ asset('img/logo_wht.png') }}">
                            <h4 class="text-center text-white bold">Grumax ERP</h4>
                        </div>

                        @isset($errors)
                            <div class="mensajes">
                                <div class="text-center text-danger">
                                    @foreach ($errors as $error)
                                        <strong>{{ $error }}</strong><br>
                                    @endforeach
                                </div>
                            </div>
                        @endisset

                        <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>

    {{-- ## --}}

    {{-- ## --}}

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
