<div class="animated" v-show="state == 0">
    <div class="form-group">
        <label for="nombre_proveedor" class="col-md-2 control-label">Nombre</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="nombre_proveedor" id="nombre_proveedor" required="required" 
                   v-model="proveedor.nombre_factura"/>
        </div>
    </div>
    <div class="form-group">
        <label for="nombrecorto" class="col-md-2 control-label">Nombre corto</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="nombrecorto" id="nombrecorto" required="required" 
                   v-model="proveedor.nombre"/>
        </div>
    </div>
    <div class="form-group">
        <label for="rfc" class="col-md-2 control-label">RFC</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="rfc" id="rfc" required="required"
                   v-model="proveedor.rfc"/>
        </div>
    </div>
    <div class="form-group">
        <label for="tipo" class="col-md-2 control-label">Tipo de proveedor</label>
        <div class="col-md-8">
            <select name="tipo" id="tipo" class="form-control" v-model="proveedor.tipo">
                <option value="Nacional">Nacional</option>
                <option value="Internacional">Internacional</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="transporte" class="col-md-2 control-label">Transportista</label>
        <div class="col-md-8">
            <input type="hidden" name="transporte" :value="proveedor.isTransporte">
            
            <label>
                <input id="transporte" type="checkbox" v-model="proveedor.isTransporte">
                <span></span>
            </label>
        </div>
    </div>
    
    <div class="row form-group">
        <label for="tipo_pago" class="col-md-2 text-right">Tipo de pago</label>
        <div class="col-md-8">
            <select name="tipo_pago" id="tipo_pago" required="required" class="form-control">
                <option value="contado">De contado</option>
                <option value="entrega">A la entrega</option>
                <option value="credito">A crédito</option>
                <option value="anticipado">Anticipado</option>
            </select>
        </div>
    </div>

    {{-- 
    <div class="row form-group">
        <label for="dias_naturales" class="col-md-2 text-right">Dias naturales de pago</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="dias_naturales" value="">
        </div>
    </div>
    <div class="row form-group">
        <label for="limite_credito" class="col-md-2 text-right">Limite de credito</label>
        <div class="col-md-8">
            <input type="text" class="form-control" value="" name="limite_credito">
        </div>
    </div>
     --}}

    <div class="row form-group">
        <label for="moneda" class="col-md-2 text-right">Moneda</label>
        <div class="col-md-8">
            <select name="moneda" id="moneda" required="required" class="form-control" v-model="proveedor.id_moneda">
                @foreach ($monedas as $moneda)
                    <option value="{{ $moneda->id }}">{{ $moneda->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row form-group">
        <label for="iva" class="col-md-2 text-right">IVA</label>
        <div class="col-md-8">
            <select name="iva" id="iva" required="required" class="form-control" v-model="proveedor.iva">
                <option value="0">No maneja IVA</option>
                <option value="16">16 %</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="comentario_proveedor" class="col-md-2 control-label">Comentario</label>
        <div class="col-md-8">
            <textarea name="comentario_proveedor" class="form-control" v-model="proveedor.comentario"></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-8">
            <h4>Dirección del proveedor</h4>
        </div>
    </div>

    <input type="hidden" name="id_direccion_recoleccion" v-model="proveedor.direccion_factura.id">
    
    <div class="form-group">
        <label for="calle" class="col-md-2 control-label">Calle</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="calle" v-model="proveedor.direccion_factura.calle"/>
        </div>
    </div>
    <div class="form-group">
        <label for="colonia" class="col-md-2 control-label">Colonia</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="colonia" v-model="proveedor.direccion_factura.colonia"/>
        </div>
    </div>
    <div class="form-group">
        <label for="numero" class="col-md-2 control-label">Número</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="numero" v-model="proveedor.direccion_factura.numero"/>
        </div>
        <label for="cp" class="col-md-2 control-label">C.P.</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="cp" v-model="proveedor.direccion_factura.cp"/>
        </div>
    </div>
    <div class="form-group">
        <label for="municipio" class="col-md-2 control-label">Municipio</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="municipio" v-model="proveedor.direccion_factura.municipio"/>
        </div>
    </div>
    <div class="form-group">
        <label for="estado" class="col-md-2 control-label">Estado</label>
        <div class="col-md-8">
            <input type="text" class="form-control" name="estado" v-model="proveedor.direccion_factura.estado"/>
        </div>
    </div>
    <div class="form-group">
        <label for="pais" class="col-md-2 control-label">País</label>
        <div class="col-md-8">
            <select class="form-control" name="pais" v-model="proveedor.direccion_factura.pais">
                <option value="" disabled selected>Seleccione un país</option>
                {{-- {% set cont = loop.index0 %}
                {% for pais in paises %}
                    <option value="{{ pais.Name }}" {% if direccion_proveedor.pais == pais.Name %}selected{% endif %}>{{ pais.Name }}</option>
                {% endfor %} --}}
            </select>
        </div>
    </div>
</div>