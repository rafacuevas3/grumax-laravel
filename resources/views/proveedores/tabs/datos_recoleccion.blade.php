<div class="animated" v-show="state == 1">
    <div class="form-group">
        <div class="col-md-8 col-md-offset-2">
            <label>
                <input type="checkbox" id="check-embarque" name="isEmbarque" v-model="proveedor.isEmbarque">
                <span></span>
                Direcciones de recolección pendientes
            </label>
        </div>
    </div>
    <div class="form-group" v-if="!proveedor.isEmbarque">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="box-group" id="accordion_direccion">
                <direccion v-for="(direccion, index) in proveedor.direcciones_recoleccion" 
                          :direccion="direccion" 
                          :index="index"
                          @remove="removeItem">
                </direccion>
            </div>
        </div>
    </div>

    <div class="form-group" v-else>
        <div class="col-md-8 col-md-offset-2">
            <div class="callout callout-warning">
                <h4>La dirección de entrega esta pendiente</h4>
                <p>Recuerda regresar y terminar el registro para no afectar el funcionamiento óptimo del sistema.</p>
            </div>
        </div>
    </div>
</div>