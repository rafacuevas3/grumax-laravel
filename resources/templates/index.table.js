Vue.component('grid', require('../../components/Table.vue'));

const app = new Vue({
  el: '#app',
  data: {
    gridColumns: [
        // Module columns for table
        {
            title: 'Nombre',
            key: 'nombre',
            url: '$MODULE$',
            url_key: 'id',
        },
    ],
    buttons: [
        { icon: 'fa-check', url: '$MODULE$', key: 'id' },
        { icon: 'fa-pencil', url: '$MODULE$/editar', key: 'id' },
        { icon: 'fa-remove', action: 'delete' },
    ],
    routes: {
        delete: { url: '/$MODULE$', key: 'id' },
        get: '/$MODULE$/all',
    }
  },
})