/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("input", {
      directives: [
        {
          name: "model",
          rawName: "v-model",
          value: _vm.contacto.id,
          expression: "contacto.id"
        }
      ],
      attrs: { type: "hidden", name: "id_contacto[]" },
      domProps: { value: _vm.contacto.id },
      on: {
        input: function($event) {
          if ($event.target.composing) {
            return
          }
          _vm.contacto.id = $event.target.value
        }
      }
    }),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-xs btn-danger pull-right",
        attrs: { type: "button" },
        on: { click: _vm.remove }
      },
      [_c("i", { staticClass: "fa fa-remove" }), _vm._v("   Remover\n    ")]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c(
        "label",
        {
          staticClass: "col-md-2 control-label",
          attrs: { for: "nombre_contacto" }
        },
        [_vm._v("Nombre")]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-8" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.contacto.nombre,
              expression: "contacto.nombre"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            name: "nombre_contacto[]",
            required: "required"
          },
          domProps: { value: _vm.contacto.nombre },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.contacto.nombre = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c(
        "label",
        {
          staticClass: "col-md-2 control-label",
          attrs: { for: "departamento" }
        },
        [_vm._v("Departamento")]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-8" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.contacto.departamento,
              expression: "contacto.departamento"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", name: "departamento[]" },
          domProps: { value: _vm.contacto.departamento },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.contacto.departamento = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c(
        "label",
        { staticClass: "control-label col-md-2", attrs: { for: "telefono" } },
        [_vm._v("Telefono(s)")]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-8" },
        _vm._l(_vm.contacto.telefonos, function(telefono, i) {
          return _c("div", { staticClass: "row form-group" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "input-group" }, [
                _c("div", { staticClass: "input-group-btn" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-default dropdown-toggle",
                      attrs: { type: "button", "data-toggle": "dropdown" }
                    },
                    [
                      _vm._v(
                        "\n                                " +
                          _vm._s(_vm._f("capitalize")(telefono.tipo)) +
                          "  "
                      ),
                      _c("span", { staticClass: "caret" })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "ul",
                    {
                      staticClass: "dropdown-menu",
                      staticStyle: { cursor: "pointer" }
                    },
                    [
                      _c("li", [
                        _c(
                          "a",
                          {
                            on: {
                              click: function($event) {
                                _vm.tipo(telefono, "casa")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-home" }),
                            _vm._v(
                              "  Casa\n                                    "
                            )
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c(
                          "a",
                          {
                            on: {
                              click: function($event) {
                                _vm.tipo(telefono, "movil")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-mobile-phone" }),
                            _vm._v(
                              "  Movil\n                                    "
                            )
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c(
                          "a",
                          {
                            on: {
                              click: function($event) {
                                _vm.tipo(telefono, "oficina")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-phone" }),
                            _vm._v(
                              "  Oficina\n                                    "
                            )
                          ]
                        )
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: telefono.telefono,
                      expression: "telefono.telefono"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    name: "telefonos[" + _vm.index + "][]"
                  },
                  domProps: { value: telefono.telefono },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      telefono.telefono = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: telefono.id,
                      expression: "telefono.id"
                    }
                  ],
                  attrs: {
                    type: "hidden",
                    name: "id_telefono[" + _vm.index + "][]"
                  },
                  domProps: { value: telefono.id },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      telefono.id = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: telefono.tipo,
                      expression: "telefono.tipo"
                    }
                  ],
                  attrs: {
                    type: "hidden",
                    name: "tipo_telefono[" + _vm.index + "][]"
                  },
                  domProps: { value: telefono.tipo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      telefono.tipo = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "input-group-addon" }, [
                  telefono.tipo == "casa"
                    ? _c("i", { staticClass: "icono fa fa-home" })
                    : telefono.tipo == "movil"
                      ? _c("i", { staticClass: "icono fa fa-mobile-phone" })
                      : _c("i", { staticClass: "icono fa fa-phone" })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group-btn" }, [
                  !i
                    ? _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "button" },
                          on: { click: _vm.addTelefono }
                        },
                        [_c("i", { staticClass: "fa fa-plus" })]
                      )
                    : _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              _vm.removeTelefono(i)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fa fa-remove" })]
                      )
                ])
              ])
            ])
          ])
        })
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c(
        "label",
        { staticClass: "col-sm-2 control-label", attrs: { for: "correo" } },
        [_vm._v("Correo(s)")]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-8", attrs: { id: "correos" } },
        _vm._l(_vm.contacto.correos, function(correo, i) {
          return _c("div", { staticClass: "row form-group" }, [
            _c("div", { staticClass: "col-sm-12" }, [
              _c("div", { staticClass: "input-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: correo.id,
                      expression: "correo.id"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "hidden",
                    name: "id_correo[" + _vm.index + "][]"
                  },
                  domProps: { value: correo.id },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      correo.id = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: correo.correo,
                      expression: "correo.correo"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "email",
                    name: "correos[" + _vm.index + "][]"
                  },
                  domProps: { value: correo.correo },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      correo.correo = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _vm._m(0, true),
                _vm._v(" "),
                _c("div", { staticClass: "input-group-btn" }, [
                  !i
                    ? _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "button" },
                          on: { click: _vm.addCorreo }
                        },
                        [_c("i", { staticClass: "fa fa-plus" })]
                      )
                    : _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              _vm.removeCorreo(i)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fa fa-remove" })]
                      )
                ])
              ])
            ])
          ])
        })
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "icono fa fa-envelope" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3ca89bc6", module.exports)
  }
}

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    data: {
        state: 0,
        titles: [{}, {}, {}]
    },
    methods: {
        setState: function setState(val) {
            this.state = val;
            var w = 100 / this.titles.length;
            var index = this.state;
            $('.step-progress').width(w * index + w / 2 + '%');
        },
        moveState: function moveState(val) {
            this.state += val;

            var w = 100 / this.titles.length;

            var index = this.state;

            $('.step-progress').width(w * index + w / 2 + '%');
        }
    }
});

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(36);


/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mixins_steps_js__ = __webpack_require__(11);
Vue.filter('capitalize', function (str) {
    return str[0].toUpperCase() + str.slice(1);
});

Vue.filter('max', function (str, max) {
    return str.substr(0, max).trim();
});

Vue.filter('parseBool', function (val) {
    return val ? 1 : 0;
});

Vue.component('direccion', __webpack_require__(5));

Vue.component('contacto', __webpack_require__(8));



var app = new Vue({
    el: '#app',
    mixins: [__WEBPACK_IMPORTED_MODULE_0__mixins_steps_js__["a" /* default */]],
    data: {
        cliente: {
            nombre: "",
            nombre_factura: "",
            nombre_oficial: "",

            comentario: "",

            direcciones: [{ pais: 'Mexico' }],
            contactos: [{ telefonos: [{ tipo: 'oficina' }], correos: [{}] }],

            direccion_factura: {},

            direcciones_recoleccion: [{}],

            isEmbarque: 0,
            rfc: "",
            tipo: "Nacional"
        }
    },
    methods: {
        addDireccion: function addDireccion() {
            this.cliente.direcciones.push({ pais: 'Mexico' });
        },
        addContacto: function addContacto() {
            this.cliente.contactos.push({ telefonos: [{ tipo: 'oficina' }], correos: [{}] });
        },
        removeItem: function removeItem(index, prop) {
            this.cliente[prop].splice(index, 1);
        },

        initData: function initData(id) {
            axios.post('/api/clientes', { id: id }).then(function (response) {
                app.cliente = response.data.cliente;
            }).catch(function (error) {
                console.log(error.response);
            });
        }
    },
    mounted: function mounted() {
        var url = window.location.href.split("/");

        if (url.indexOf('editar') !== -1) {
            this.initData(url[url.length - 1]);
        }
    }
});

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(6)
/* template */
var __vue_template__ = __webpack_require__(7)
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/Direccion.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Direccion.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0212ab49", Component.options)
  } else {
    hotAPI.reload("data-v-0212ab49", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['direccion', 'index'],
    methods: {
        remove: function remove() {
            this.$emit('remove', this.index, 'direcciones_recoleccion');
        }
    }
});

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "panel box box-success border-flat with-border" },
    [
      _c(
        "div",
        {
          staticClass: "box-header panel-heading-hover with-border",
          attrs: {
            "data-toggle": "collapse",
            "data-parent": "#accordion_direccion",
            href: "#direccion" + _vm.index
          }
        },
        [
          _c("h4", { staticClass: "box-title" }, [
            _vm._v(
              "\n            Dirección #" +
                _vm._s(_vm.index + 1) +
                "\n            "
            ),
            _c(
              "small",
              {
                staticClass: "text-muted",
                staticStyle: { "margin-left": "10px" }
              },
              [_vm._v("Direcciones")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close box-close-button",
          attrs: { type: "button" },
          on: { click: _vm.remove }
        },
        [_c("span", [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "panel-collapse collapse",
          attrs: { id: "direccion" + _vm.index }
        },
        [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.direccion.id,
                expression: "direccion.id"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "hidden", name: "id_direccion[]" },
            domProps: { value: _vm.direccion.id },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.direccion.id = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "box-body" }, [
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "nombre_direccion" }
                },
                [_vm._v("Nombre dirección")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.nombre,
                      expression: "direccion.nombre"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "nombre_direccion[]" },
                  domProps: { value: _vm.direccion.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.nombre = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "comentario" }
                },
                [_vm._v("Comentario")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.comentario,
                      expression: "direccion.comentario"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "comentario_direccion[]" },
                  domProps: { value: _vm.direccion.comentario },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.comentario = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "calle" }
                },
                [_vm._v("Calle")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.calle,
                      expression: "direccion.calle"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "calle_direccion[]" },
                  domProps: { value: _vm.direccion.calle },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.calle = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "colonia" }
                },
                [_vm._v("Colonia")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.colonia,
                      expression: "direccion.colonia"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "colonia_direccion[]" },
                  domProps: { value: _vm.direccion.colonia },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.colonia = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "numero" }
                },
                [_vm._v("Número")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-4" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.numero,
                      expression: "direccion.numero"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "numero_direccion[]" },
                  domProps: { value: _vm.direccion.numero },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.numero = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "label",
                { staticClass: "col-md-2 control-label", attrs: { for: "cp" } },
                [_vm._v("C.P.")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-4" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.cp,
                      expression: "direccion.cp"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "cp_direccion[]" },
                  domProps: { value: _vm.direccion.cp },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.cp = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "municipio" }
                },
                [_vm._v("Municipio")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.municipio,
                      expression: "direccion.municipio"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "municipio_direccion[]" },
                  domProps: { value: _vm.direccion.municipio },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.municipio = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "estado" }
                },
                [_vm._v("Estado")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.estado,
                      expression: "direccion.estado"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", name: "estado_direccion[]" },
                  domProps: { value: _vm.direccion.estado },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.estado = $event.target.value
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "pais" }
                },
                [_vm._v("País")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("select", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.pais,
                      expression: "direccion.pais"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "pais_direccion[]" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.direccion.pais = $event.target.multiple
                        ? $$selectedVal
                        : $$selectedVal[0]
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  staticClass: "col-md-2 control-label",
                  attrs: { for: "telefono" }
                },
                [_vm._v("Teléfono")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-10" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.direccion.telefono,
                      expression: "direccion.telefono"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    name: "telefono_direccion[]",
                    maxlength: "25"
                  },
                  domProps: { value: _vm.direccion.telefono },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.direccion.telefono = $event.target.value
                    }
                  }
                })
              ])
            ])
          ])
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0212ab49", module.exports)
  }
}

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(9)
/* template */
var __vue_template__ = __webpack_require__(10)
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/Contacto.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Contacto.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ca89bc6", Component.options)
  } else {
    hotAPI.reload("data-v-3ca89bc6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['contacto', 'index'],
    methods: {
        remove: function remove() {
            this.$emit('remove', this.index, 'contactos');
        },
        addTelefono: function addTelefono() {
            this.contacto.telefonos.push({ tipo: 'oficina' });
        },
        addCorreo: function addCorreo() {
            this.contacto.correos.push({});
        },

        removeTelefono: function removeTelefono(i) {
            this.contacto.telefonos.splice(i, 1);
        },
        removeCorreo: function removeCorreo(i) {
            this.contacto.correos.splice(i, 1);
        },
        tipo: function tipo(telefono, val) {
            telefono.tipo = val;
        }
    }
});

/***/ })

/******/ });