/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 29);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(2)
/* template */
var __vue_template__ = __webpack_require__(3)
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/Table.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Table.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4bbe0367", Component.options)
  } else {
    hotAPI.reload("data-v-4bbe0367", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        columns: Array,
        buttons: Array,
        routes: Object
    },
    data: function data() {
        var sortOrders = {};

        this.columns.forEach(function (column) {
            sortOrders[column.key] = 1;
        });

        return {
            data: [],
            filterKey: '',
            sortKey: '',
            sortOrders: sortOrders
        };
    },
    computed: {
        filteredData: function filteredData() {
            var sortKey = this.sortKey;
            var filterKey = this.filterKey && this.filterKey.toLowerCase();
            var order = this.sortOrders[sortKey] || 1;
            var data = this.data;
            if (filterKey) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1;
                    });
                });
            }
            if (sortKey) {
                data = data.slice().sort(function (a, b) {
                    a = a[sortKey];
                    b = b[sortKey];
                    return (a === b ? 0 : a > b ? 1 : -1) * order;
                });
            }
            return data;
        },
        hasButtons: function hasButtons() {
            return typeof this.buttons !== 'undefined' && this.buttons.length > 0;
        }
    },
    filters: {
        capitalize: function capitalize(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    },
    methods: {
        sortBy: function sortBy(key) {
            this.sortKey = key;
            this.sortOrders[key] = this.sortOrders[key] * -1;
        },

        triggerAction: function triggerAction(button, entry, index, event) {
            if (typeof button.action !== 'undefined') {
                event.preventDefault();

                if (typeof this[button.action] === 'function') {
                    this[button.action](entry, index, button.args || []);
                }

                return true;

                this.$emit(button.action, entry, index, button.args || []);
            }
        },
        compileUrl: function compileUrl(url, id) {
            if (typeof url === 'undefined') {
                return '';
            }

            return url + '/' + id;
        },
        delete: function _delete(entry, index, args) {
            var vo = this;

            var kStr = this.routes.delete.key ? '/' + entry[this.routes.delete.key] : '';

            swal({
                title: this.routes.delete.title || '¿Deseas continuar con la operación?',
                text: this.routes.delete.text || 'Este cambio no podrá revertirse',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar'
            }).then(function () {
                axios.delete(vo.routes.delete.url + kStr).then(function (response) {
                    swal('¡Registro eliminado!', '', 'success');

                    vo.data.splice(index, 1);
                }).catch(function (error) {
                    swal('¡Error en el servidor!', 'Intentalo más tarde', 'error');

                    console.log(error.response);
                });
            });
        }
    },
    mounted: function mounted() {
        var vo = this;

        axios.post(this.routes.get).then(function (response) {
            vo.data = response.data.data;
        }).catch(function (error) {
            console.log(error.response);
        });
    }
});

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(30);


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.data.length > 0
      ? _c("div", [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.filterKey,
                expression: "filterKey"
              }
            ],
            staticClass: "search-input pull-right",
            attrs: { name: "query" },
            domProps: { value: _vm.filterKey },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.filterKey = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass: "text-bold pull-right",
              staticStyle: { "margin-right": "10px" }
            },
            [_vm._v("Búsqueda:")]
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.data.length > 0
      ? _c("div", { staticClass: "relative-layout" }, [
          _c("div", { staticClass: "fixed-container" }, [
            _c("table", { staticClass: "fixed-table" }, [
              _c("thead", [
                _c(
                  "tr",
                  [
                    _vm._l(_vm.columns, function(column) {
                      return _c(
                        "th",
                        {
                          on: {
                            click: function($event) {
                              _vm.sortBy(column.key)
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                            \n                            " +
                              _vm._s(column.title) +
                              "\n\n                            "
                          ),
                          _c(
                            "div",
                            { class: { active: _vm.sortKey == column.key } },
                            [
                              _vm._v(
                                "\n                                " +
                                  _vm._s(column.title) +
                                  "\n                                \n                                "
                              ),
                              _c("i", {
                                staticClass: "fa",
                                class:
                                  _vm.sortOrders[column.key] > 0
                                    ? "fa-sort-asc"
                                    : "fa-sort-desc"
                              })
                            ]
                          )
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _vm.hasButtons
                      ? _c("th", { staticStyle: { width: "250px" } }, [
                          _vm._v(
                            "\n                            Acciones\n\n                            "
                          ),
                          _c("div", { staticStyle: { width: "250px" } }, [
                            _vm._v("Acciones")
                          ])
                        ])
                      : _vm._e()
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.filteredData, function(entry, index) {
                  return _c(
                    "tr",
                    [
                      _vm._l(_vm.columns, function(column) {
                        return _c("td", [
                          column.url
                            ? _c("a", {
                                attrs: {
                                  href: _vm.compileUrl(
                                    column.url,
                                    entry[column.url_key]
                                  )
                                },
                                domProps: {
                                  textContent: _vm._s(entry[column.key])
                                }
                              })
                            : _c("span", {
                                domProps: {
                                  textContent: _vm._s(entry[column.key])
                                }
                              })
                        ])
                      }),
                      _vm._v(" "),
                      _vm.hasButtons
                        ? _c(
                            "td",
                            { staticClass: "actions" },
                            _vm._l(_vm.buttons, function(button) {
                              return _c(
                                "a",
                                {
                                  staticClass: "btn btn-default btn-xs flat",
                                  attrs: {
                                    href: _vm.compileUrl(
                                      button.url,
                                      entry[button.key]
                                    )
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.triggerAction(
                                        button,
                                        entry,
                                        index,
                                        $event
                                      )
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa ",
                                    class: button.icon
                                  })
                                ]
                              )
                            })
                          )
                        : _vm._e()
                    ],
                    2
                  )
                })
              )
            ])
          ])
        ])
      : _c("h4", [_vm._v("No hay registros")])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4bbe0367", module.exports)
  }
}

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('grid', __webpack_require__(1));

var app = new Vue({
    el: '#app',
    data: {
        gridColumns: [{
            title: 'Nombre',
            key: 'nombre',
            url: 'categorias',
            url_key: 'id'
        }, {
            title: 'Descripcion',
            key: 'descripcion'
        }],
        buttons: [{ icon: 'fa-check', url: 'categorias', key: 'id' }, { icon: 'fa-pencil', url: 'categorias/editar', key: 'id' }, { icon: 'fa-remove', action: 'delete' }],
        routes: {
            delete: { url: '/categorias', key: 'id' },
            get: '/categorias/all'
        }
    }
});

/***/ })

/******/ });