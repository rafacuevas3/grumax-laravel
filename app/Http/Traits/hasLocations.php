<?php 

namespace App\Http\Traits;

use App\Models\Direccion;

trait hasLocations {

    public function direcciones()
    {
         return $this->morphMany('App\Models\Direccion', 'locations');
    }

    public function addDireccion($data)
    {
        return $this->direcciones()->create($data);
    }

    public function updateDireccion($id, $data)
    {
        return $this->direcciones()->updateOrCreate(['id' => $id], $data);
    }

    public function deleteDirecciones($old)
    {
        return \DB::table('direcciones')
                  ->where('locations_id', $this->id)
                  ->where('locations_type', static::class)
                  ->whereNotIn('id', $old)
                  ->delete();
    }

    public function direccion_factura()
    {
        return $this->direcciones->where('tipo_direccion', Direccion::FACTURA)->first();
    }

    public function direcciones_recoleccion()
    {
        return $this->direcciones->where('tipo_direccion', Direccion::RECOLECCION);
    }
}