<?php 

namespace App\Http\Traits;

use App\Models\Contacto;

trait hasContacts {

    public function contactos()
    {
         return $this->morphMany('App\Models\Contacto', 'contactable');
    }

    public function addContact($data)
    {
        return $this->contactos()->create($data);
    }

    public function updateContact($id, $data)
    {
        return $this->contactos()->updateOrCreate(['id' => $id], $data);
    }

    public function deleteContactos($old)
    {
        $contactos = \DB::table('contactos')
                        ->where('contactable_id', $this->id)
                        ->where('contactable_type', static::class)
                        ->whereNotIn('id', $old)
                        ->get();

        \DB::table('telefonos')
            ->whereIn('id_contacto', $contactos->pluck('id')->all())
            ->delete();

        \DB::table('correos')
            ->whereIn('id_contacto', $contactos->pluck('id')->all())
            ->delete();

        \DB::table('contactos')
            ->where('contactable_id', $this->id)
            ->where('contactable_type', static::class)
            ->whereNotIn('id', $old)
            ->delete();
    }
}