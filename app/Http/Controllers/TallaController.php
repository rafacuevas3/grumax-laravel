<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTallaRequest;
use App\Http\Requests\UpdateTallaRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Talla;

class TallaController extends AppBaseController
{

    /**
     * Display a listing of the Talla.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tallas = Talla::all();

        return view('tallas.index')
             ->with('tallas', $tallas);
    }

    /**
     * Get a listing of the Talla.
     *
     * @param void
     * @return Array
     */
    public function all()
    {
        return [
            'data' => Talla::all()
        ];
    }

    /**
     * Show the form for creating a new Talla.
     *
     * @return Response
     */
    public function create()
    {
        return view('tallas.create');
    }

    /**
     * Store a newly created Talla in storage.
     *
     * @param CreateTallaRequest $request
     *
     * @return Response
     */
    public function store(CreateTallaRequest $request)
    {
        $input = $request->all();

        $talla = Talla::create($input);

        Flash::success('Talla saved successfully.');

        return redirect(route('tallas.index'));
    }

    /**
     * Display the specified Talla.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $talla = Talla::find($id);

        if (empty($talla)) {
            Flash::error('Talla not found');

            return redirect(route('tallas.index'));
        }

        return view('tallas.show')->with('talla', $talla);
    }

    /**
     * Show the form for editing the specified Talla.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $talla = Talla::find($id);

        if (empty($talla)) {
            Flash::error('Talla not found');

            return redirect(route('tallas.index'));
        }

        return view('tallas.edit')->with('talla', $talla);
    }

    /**
     * Update the specified Talla in storage.
     *
     * @param  int              $id
     * @param UpdateTallaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTallaRequest $request)
    {
        $talla = Talla::find($id);

        if (empty($talla)) {
            Flash::error('Talla not found');

            return redirect(route('tallas.index'));
        }

        $talla->fill($request->all());

        $talla->save();

        Flash::success('Talla updated successfully.');

        return redirect(route('tallas.index'));
    }

    /**
     * Remove the specified Talla from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $talla = Talla::findOrFail($id);

        return [
            "response" => $talla->delete()
        ];
    }
}
