<?php

namespace App\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Proveedor;
use App\Models\Moneda;
use App\Models\Direccion;

class UpdateController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedores.create')->with(['monedas' => Moneda::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        
        $proveedor = new Proveedor([
            'id_moneda'      => $request->get('moneda'),
            'nombre'         => $request->get('nombrecorto'),
            'nombre_oficial' => $request->get('nombre_proveedor'),
            'nombre_factura' => $request->get('nombre_proveedor'),
            'comentario'     => $request->get('comentario_proveedor'),
            'rfc'            => $request->get('rfc'),
            'tipo'           => $request->get('tipo'),
            'iva'            => $request->get('iva'),
            'isTransporte'   => $request->get('transporte') ?? 0,
            'isEmbarque'     => $request->get('isEmbarque') ?? 0,
        ]);

        $proveedor->save();

        foreach ($request->get('nombre_contacto') as $i => $nombre) {
            $contacto = $proveedor->addContact([
                'nombre' => $nombre,
                'departamento' => $request->input("departamento.$i"),
                'puesto' => '',
            ]);

            foreach ($request->input("telefonos.$i") as $j => $telefono) {
                $contacto->addTelefono([
                    'telefono' => $telefono,
                    'tipo' => $request->input("tipo_telefono.$i.$j") ?? 'oficina',
                ]);
            }

            foreach ($request->input("correos.$i") as $j => $correo) {
                $contacto->addCorreo(['correo' => $correo]);
            }

        }

        $proveedor->addDireccion([
            'calle'     => $request->get('calle'),
            'colonia'   => $request->get('colonia'),
            'numero'    => $request->get('numero'),
            'cp'        => $request->get('cp'),
            'municipio' => $request->get('municipio'),
            'estado'    => $request->get('estado'),
            'telefono'  => $request->get('telefono'),
            'tipo_direccion' => Direccion::FACTURA,
        ]);

        foreach ($request->get('nombre_direccion') as $i => $nombre) {
            $proveedor->addDireccion([
                'nombre'     => $nombre,
                'comentario' => $request->get('comentario_direccion')[$i],
                'calle'     => $request->get('calle_direccion')[$i],
                'colonia'   => $request->get('colonia_direccion')[$i],
                'numero'    => $request->get('numero_direccion')[$i],
                'cp'        => $request->get('cp_direccion')[$i],
                'municipio' => $request->get('municipio_direccion')[$i],
                'estado'    => $request->get('estado_direccion')[$i],
                'telefono'  => $request->get('telefono_direccion')[$i],
                'tipo_direccion' => Direccion::RECOLECCION,
            ]);
        }

        \DB::commit();

        return redirect()->route('proveedores.show', ['proveedor' => $proveedor->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit(Proveedor $proveedor)
    {
        return view('proveedores.edit')->with(['proveedor' => $proveedor, 'monedas' => Moneda::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proveedor $proveedor)
    {
        \DB::beginTransaction();
        
        $proveedor->update([
            'id_moneda'      => $request->get('moneda'),
            'nombre'         => $request->get('nombrecorto'),
            'nombre_oficial' => $request->get('nombre_proveedor'),
            'nombre_factura' => $request->get('nombre_proveedor'),
            'comentario'     => $request->get('comentario_proveedor'),
            'rfc'            => $request->get('rfc'),
            'tipo'           => $request->get('tipo'),
            'iva'            => $request->get('iva'),
            'isTransporte'   => $request->get('transporte') ?? 0,
            'isEmbarque'     => $request->get('isEmbarque') ?? 0,
        ]);
        
        $proveedor->deleteContactos($request->get("id_contacto"));

        foreach ($request->get('nombre_contacto') as $i => $nombre) {
            $contacto = $proveedor->updateContact($request->input("id_contacto.$i"), [
                'nombre' => $nombre,
                'departamento' => $request->input("departamento.$i"),
                'puesto' => '',
            ]);

            // Telefonos
            $contacto->deleteTelefonos($request->input("id_telefono.$i"));
            
            foreach ($request->input("telefonos.$i") as $j => $telefono) {
                $contacto->updateTelefono($request->input("id_telefono.$i.$j"), [
                    'telefono' => $telefono,
                    'tipo' => $request->input("tipo_telefono.$i.$j") ?? 'oficina',
                ]);
            }

            // Correos
            $contacto->deleteCorreos($request->input("id_correo.$i"));
            
            foreach ($request->input("correos.$i") as $j => $correo) {
                $contacto->updateCorreo($request->input("id_correo.$i.$j"), [
                    'correo' => $correo
                ]);
            }
        }

        $old = array_merge([$request->get('id_direccion_recoleccion')], $request->get('id_direccion'));

        $proveedor->deleteDirecciones($old);

        $proveedor->updateDireccion($request->get('id_direccion_recoleccion'), [
            'calle'          => $request->get('calle'),
            'colonia'        => $request->get('colonia'),
            'numero'         => $request->get('numero'),
            'cp'             => $request->get('cp'),
            'municipio'      => $request->get('municipio'),
            'estado'         => $request->get('estado'),
            'telefono'       => $request->get('telefono'),
            'tipo_direccion' => Direccion::FACTURA,
        ]);


        foreach ($request->get('nombre_direccion') as $i => $nombre) {
            $proveedor->updateDireccion($request->input("id_direccion.$i"), [
                'nombre'         => $nombre,
                'comentario'     => $request->input("comentario_direccion.$i"),
                'calle'          => $request->input("calle_direccion.$i"),
                'colonia'        => $request->input("colonia_direccion.$i"),
                'numero'         => $request->input("numero_direccion.$i"),
                'cp'             => $request->input("cp_direccion.$i"),
                'municipio'      => $request->input("municipio_direccion.$i"),
                'estado'         => $request->input("estado_direccion.$i"),
                'telefono'       => $request->input("telefono_direccion.$i"),
                'tipo_direccion' => Direccion::RECOLECCION,
            ]);
        }

        \DB::commit();

        return redirect()->route('proveedores.show', ['proveedor' => $proveedor->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        
        return [
            "response" => $proveedor->delete()
        ];
    }
}
