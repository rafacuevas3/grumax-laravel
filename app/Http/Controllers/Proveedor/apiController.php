<?php

namespace App\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Proveedor;
use App\Models\Direccion;

class apiController extends Controller
{
    public function __invoke(Request $request)
    {
        $proveedor = Proveedor::with(['contactos', 'direcciones', 'contactos.correos', 'contactos.telefonos'])
                              ->where('id', $request->get('id'))->firstOrFail();

        $proveedor->direccion_factura = $proveedor->direcciones->where('tipo_direccion', Direccion::FACTURA)->first();
        $proveedor->direcciones_recoleccion = $proveedor->direcciones->where('tipo_direccion', Direccion::RECOLECCION)->values();
        
        return [
            'proveedor' => $proveedor
        ];
    }
}
