<?php

namespace App\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Proveedor;

class apiListController extends Controller
{
    public function __invoke()
    {
        return [
            'data' => Proveedor::all()
        ];
    }
}
