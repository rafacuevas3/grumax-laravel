<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMonedaRequest;
use App\Http\Requests\UpdateMonedaRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Moneda;

class MonedaController extends AppBaseController
{

    /**
     * Display a listing of the Moneda.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $monedas = Moneda::all();

        return view('monedas.index')
             ->with('monedas', $monedas);
    }

    /**
     * Get a listing of the Moneda.
     *
     * @param void
     * @return Array
     */
    public function all()
    {
        return [
            'data' => Moneda::all()
        ];
    }

    /**
     * Show the form for creating a new Moneda.
     *
     * @return Response
     */
    public function create()
    {
        return view('monedas.create');
    }

    /**
     * Store a newly created Moneda in storage.
     *
     * @param CreateMonedaRequest $request
     *
     * @return Response
     */
    public function store(CreateMonedaRequest $request)
    {
        $input = $request->all();

        $moneda = Moneda::create($input);

        Flash::success('Moneda saved successfully.');

        return redirect(route('monedas.index'));
    }

    /**
     * Display the specified Moneda.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $moneda = Moneda::find($id);

        if (empty($moneda)) {
            Flash::error('Moneda not found');

            return redirect(route('monedas.index'));
        }

        return view('monedas.show')->with('moneda', $moneda);
    }

    /**
     * Show the form for editing the specified Moneda.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $moneda = Moneda::find($id);

        if (empty($moneda)) {
            Flash::error('Moneda not found');

            return redirect(route('monedas.index'));
        }

        return view('monedas.edit')->with('moneda', $moneda);
    }

    /**
     * Update the specified Moneda in storage.
     *
     * @param  int              $id
     * @param UpdateMonedaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMonedaRequest $request)
    {
        $moneda = Moneda::find($id);

        if (empty($moneda)) {
            Flash::error('Moneda not found');

            return redirect(route('monedas.index'));
        }

        $moneda->fill($request->all());

        $moneda->save();

        Flash::success('Moneda updated successfully.');

        return redirect(route('monedas.index'));
    }

    /**
     * Remove the specified Moneda from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $moneda = Moneda::findOrFail($id);

        return [
            "response" => $moneda->delete()
        ];
    }
}
