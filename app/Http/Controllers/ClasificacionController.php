<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClasificacionRequest;
use App\Http\Requests\UpdateClasificacionRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Clasificacion;

class ClasificacionController extends AppBaseController
{

    /**
     * Display a listing of the Clasificacion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $clasificaciones = Clasificacion::all();

        return view('clasificaciones.index')
             ->with('clasificaciones', $clasificaciones);
    }

    /**
     * Get a listing of the Clasificacion.
     *
     * @param void
     * @return Array
     */
    public function all()
    {
        return [
            'data' => Clasificacion::all()
        ];
    }

    /**
     * Show the form for creating a new Clasificacion.
     *
     * @return Response
     */
    public function create()
    {
        return view('clasificaciones.create');
    }

    /**
     * Store a newly created Clasificacion in storage.
     *
     * @param CreateClasificacionRequest $request
     *
     * @return Response
     */
    public function store(CreateClasificacionRequest $request)
    {
        $input = $request->all();

        $clasificacion = Clasificacion::create($input);

        Flash::success('Clasificacion saved successfully.');

        return redirect(route('clasificaciones.index'));
    }

    /**
     * Display the specified Clasificacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clasificacion = Clasificacion::find($id);

        if (empty($clasificacion)) {
            Flash::error('Clasificacion not found');

            return redirect(route('clasificaciones.index'));
        }

        return view('clasificaciones.show')->with('clasificacion', $clasificacion);
    }

    /**
     * Show the form for editing the specified Clasificacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clasificacion = Clasificacion::find($id);

        if (empty($clasificacion)) {
            Flash::error('Clasificacion not found');

            return redirect(route('clasificaciones.index'));
        }

        return view('clasificaciones.edit')->with('clasificacion', $clasificacion);
    }

    /**
     * Update the specified Clasificacion in storage.
     *
     * @param  int              $id
     * @param UpdateClasificacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClasificacionRequest $request)
    {
        $clasificacion = Clasificacion::find($id);

        if (empty($clasificacion)) {
            Flash::error('Clasificacion not found');

            return redirect(route('clasificaciones.index'));
        }

        $clasificacion->fill($request->all());

        $clasificacion->save();

        Flash::success('Clasificacion updated successfully.');

        return redirect(route('clasificaciones.index'));
    }

    /**
     * Remove the specified Clasificacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clasificacion = Clasificacion::findOrFail($id);

        return [
            "response" => $clasificacion->delete()
        ];
    }
}
