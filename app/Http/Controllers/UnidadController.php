<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUnidadRequest;
use App\Http\Requests\UpdateUnidadRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Unidad;

class UnidadController extends AppBaseController
{

    /**
     * Display a listing of the Unidad.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $unidades = Unidad::all();

        return view('unidades.index')
             ->with('unidades', $unidades);
    }

    public function all()
    {
        return [
            'data' => Unidad::all()
        ];
    }

    /**
     * Show the form for creating a new Unidad.
     *
     * @return Response
     */
    public function create()
    {
        return view('unidades.create');
    }

    /**
     * Store a newly created Unidad in storage.
     *
     * @param CreateUnidadRequest $request
     *
     * @return Response
     */
    public function store(CreateUnidadRequest $request)
    {
        $input = $request->all();

        $unidad = Unidad::create($input);

        Flash::success('Unidad saved successfully.');

        return redirect(route('unidades.index'));
    }

    /**
     * Display the specified Unidad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unidad = Unidad::find($id);

        if (empty($unidad)) {
            Flash::error('Unidad not found');

            return redirect(route('unidades.index'));
        }

        return view('unidades.show')->with('unidad', $unidad);
    }

    /**
     * Show the form for editing the specified Unidad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $unidad = Unidad::find($id);

        if (empty($unidad)) {
            Flash::error('Unidad not found');

            return redirect(route('unidades.index'));
        }

        return view('unidades.edit')->with('unidad', $unidad);
    }

    /**
     * Update the specified Unidad in storage.
     *
     * @param  int              $id
     * @param UpdateUnidadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnidadRequest $request)
    {
        $unidad = Unidad::find($id);

        if (empty($unidad)) {
            Flash::error('Unidad not found');

            return redirect(route('unidades.index'));
        }

        $unidad->fill($request->all());

        $unidad->save();

        Flash::success('Unidad updated successfully.');

        return redirect(route('unidades.index'));
    }

    /**
     * Remove the specified Unidad from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $unidad = Unidad::findOrFail($id);
        
        return [
            "response" => $unidad->delete()
        ];
    }
}
