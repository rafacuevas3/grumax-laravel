<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Cliente;

class apiListController extends Controller
{
    public function __invoke()
    {
        return [
            'data' => Cliente::all()
        ];
    }
}
