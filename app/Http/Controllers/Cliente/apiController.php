<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Models\Direccion;

class apiController extends Controller
{
    public function __invoke(Request $request)
    {
        $cliente = Cliente::with(['contactos', 'direcciones', 'contactos.correos', 'contactos.telefonos'])
                          ->where('id', $request->get('id'))->firstOrFail();

        $cliente->direccion_factura = $cliente->direcciones->where('tipo_direccion', Direccion::FACTURA)->first();
        $cliente->direcciones_recoleccion = $cliente->direcciones->where('tipo_direccion', Direccion::RECOLECCION)->values();
        
        return [
            'cliente' => $cliente
        ];
    }
}
