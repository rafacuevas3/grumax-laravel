<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Models\Direccion;

class UpdateController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        
        $cliente = new Cliente([
            'nombre'         => $request->get('nombrecorto'),
            'nombre_oficial' => $request->get('nombre_cliente'),
            'nombre_factura' => $request->get('nombre_cliente'),
            'comentario'     => $request->get('comentario_cliente'),
            'rfc'            => $request->get('rfc'),
            'tipo'           => $request->get('tipo'),
            'isEmbarque'     => $request->get('isEmbarque') ?? 0,
            'logo'           => $request->get('logo') ?? '',
        ]);

        $cliente->save();

        foreach ($request->get('nombre_contacto') as $i => $nombre) {
            $contacto = $cliente->addContact([
                'nombre' => $nombre,
                'departamento' => $request->input("departamento.$i"),
                'puesto' => '',
            ]);

            foreach ($request->input("telefonos.$i") as $j => $telefono) {
                $contacto->addTelefono([
                    'telefono' => $telefono,
                    'tipo' => $request->input("tipo_telefono.$i.$j") ?? 'oficina',
                ]);
            }

            foreach ($request->input("correos.$i") as $j => $correo) {
                $contacto->addCorreo(['correo' => $correo]);
            }

        }

        $cliente->addDireccion([
            'calle'     => $request->get('calle'),
            'colonia'   => $request->get('colonia'),
            'numero'    => $request->get('numero'),
            'cp'        => $request->get('cp'),
            'municipio' => $request->get('municipio'),
            'estado'    => $request->get('estado'),
            'telefono'  => $request->get('telefono'),
            'tipo_direccion' => Direccion::FACTURA,
        ]);

        foreach ($request->get('nombre_direccion') as $i => $nombre) {
            $cliente->addDireccion([
                'nombre'     => $nombre,
                'comentario' => $request->get('comentario_direccion')[$i],
                'calle'     => $request->get('calle_direccion')[$i],
                'colonia'   => $request->get('colonia_direccion')[$i],
                'numero'    => $request->get('numero_direccion')[$i],
                'cp'        => $request->get('cp_direccion')[$i],
                'municipio' => $request->get('municipio_direccion')[$i],
                'estado'    => $request->get('estado_direccion')[$i],
                'telefono'  => $request->get('telefono_direccion')[$i],
                'tipo_direccion' => Direccion::RECOLECCION,
            ]);
        }

        \DB::commit();

        return redirect()->route('clientes.show', ['cliente' => $cliente->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        return view('clientes.edit')->with(['cliente' => $cliente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        \DB::beginTransaction();
        
        $cliente->update([
            'nombre'         => $request->get('nombrecorto'),
            'nombre_oficial' => $request->get('nombre_cliente'),
            'nombre_factura' => $request->get('nombre_cliente'),
            'comentario'     => $request->get('comentario_cliente'),
            'rfc'            => $request->get('rfc'),
            'tipo'           => $request->get('tipo'),
            'iva'            => $request->get('iva'),
            'isTransporte'   => $request->get('transporte') ?? 0,
            'isEmbarque'     => $request->get('isEmbarque') ?? 0,
        ]);
        
        $cliente->deleteContactos($request->get("id_contacto"));

        foreach ($request->get('nombre_contacto') as $i => $nombre) {
            $contacto = $cliente->updateContact($request->input("id_contacto.$i"), [
                'nombre' => $nombre,
                'departamento' => $request->input("departamento.$i"),
                'puesto' => '',
            ]);

            // Telefonos
            $contacto->deleteTelefonos($request->input("id_telefono.$i"));
            
            foreach ($request->input("telefonos.$i") as $j => $telefono) {
                $contacto->updateTelefono($request->input("id_telefono.$i.$j"), [
                    'telefono' => $telefono,
                    'tipo' => $request->input("tipo_telefono.$i.$j") ?? 'oficina',
                ]);
            }

            // Correos
            $contacto->deleteCorreos($request->input("id_correo.$i"));
            
            foreach ($request->input("correos.$i") as $j => $correo) {
                $contacto->updateCorreo($request->input("id_correo.$i.$j"), [
                    'correo' => $correo
                ]);
            }
        }

        $old = array_merge([$request->get('id_direccion_recoleccion')], $request->get('id_direccion'));

        $cliente->deleteDirecciones($old);

        $cliente->updateDireccion($request->get('id_direccion_recoleccion'), [
            'calle'          => $request->get('calle'),
            'colonia'        => $request->get('colonia'),
            'numero'         => $request->get('numero'),
            'cp'             => $request->get('cp'),
            'municipio'      => $request->get('municipio'),
            'estado'         => $request->get('estado'),
            'telefono'       => $request->get('telefono'),
            'tipo_direccion' => Direccion::FACTURA,
        ]);


        foreach ($request->get('nombre_direccion') as $i => $nombre) {
            $cliente->updateDireccion($request->input("id_direccion.$i"), [
                'nombre'         => $nombre,
                'comentario'     => $request->input("comentario_direccion.$i"),
                'calle'          => $request->input("calle_direccion.$i"),
                'colonia'        => $request->input("colonia_direccion.$i"),
                'numero'         => $request->input("numero_direccion.$i"),
                'cp'             => $request->input("cp_direccion.$i"),
                'municipio'      => $request->input("municipio_direccion.$i"),
                'estado'         => $request->input("estado_direccion.$i"),
                'telefono'       => $request->input("telefono_direccion.$i"),
                'tipo_direccion' => Direccion::RECOLECCION,
            ]);
        }

        \DB::commit();

        return redirect()->route('clientes.show', ['cliente' => $cliente->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        
        return [
            "response" => $cliente->delete()
        ];
    }
}
