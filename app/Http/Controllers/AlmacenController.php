<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlmacenRequest;
use App\Http\Requests\UpdateAlmacenRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Almacen;

class AlmacenController extends AppBaseController
{

    /**
     * Display a listing of the Almacen.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $almacenes = Almacen::all();

        return view('almacenes.index')
             ->with('almacenes', $almacenes);
    }

    /**
     * Get a listing of the Almacen.
     *
     * @param void
     * @return Array
     */
    public function all()
    {
        return [
            'data' => Almacen::all()
        ];
    }

    /**
     * Show the form for creating a new Almacen.
     *
     * @return Response
     */
    public function create()
    {
        return view('almacenes.create');
    }

    /**
     * Store a newly created Almacen in storage.
     *
     * @param CreateAlmacenRequest $request
     *
     * @return Response
     */
    public function store(CreateAlmacenRequest $request)
    {
        $input = $request->all();

        $almacen = Almacen::create($input);

        Flash::success('Almacen saved successfully.');

        return redirect(route('almacenes.index'));
    }

    /**
     * Display the specified Almacen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $almacen = Almacen::find($id);

        if (empty($almacen)) {
            Flash::error('Almacen not found');

            return redirect(route('almacenes.index'));
        }

        return view('almacenes.show')->with('almacen', $almacen);
    }

    /**
     * Show the form for editing the specified Almacen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $almacen = Almacen::find($id);

        if (empty($almacen)) {
            Flash::error('Almacen not found');

            return redirect(route('almacenes.index'));
        }

        return view('almacenes.edit')->with('almacen', $almacen);
    }

    /**
     * Update the specified Almacen in storage.
     *
     * @param  int              $id
     * @param UpdateAlmacenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlmacenRequest $request)
    {
        $almacen = Almacen::find($id);

        if (empty($almacen)) {
            Flash::error('Almacen not found');

            return redirect(route('almacenes.index'));
        }

        $almacen->fill($request->all());

        $almacen->save();

        Flash::success('Almacen updated successfully.');

        return redirect(route('almacenes.index'));
    }

    /**
     * Remove the specified Almacen from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $almacen = Almacen::findOrFail($id);

        return [
            "response" => $almacen->delete()
        ];
    }
}
