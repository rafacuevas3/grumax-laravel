<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateIndexVue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vue:index {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a vue table index file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        $resourse_path = 'resources/assets/js/modules/' . $name;

        $template = str_replace('$MODULE$', $name, file_get_contents('resources/templates/index.table.js'));

        if (!file_exists($resourse_path)) {
            mkdir($resourse_path, 0777, true);
        }

        file_put_contents($resourse_path.'/index.js', $template);
        
        file_put_contents('webpack.mix.js', 
                          "\nmix.js('$resourse_path/index.js', 'public/js/index.$name.js');\n", 
                          FILE_APPEND | LOCK_EX);

        $this->info('Vue index table for '. $name .' created!');
    }
}
