<?php

namespace App\Repositories;

use App\Models\Unidad;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UnidadRepository
 * @package App\Repositories
 * @version November 10, 2017, 12:41 am UTC
 *
 * @method Unidad findWithoutFail($id, $columns = ['*'])
 * @method Unidad find($id, $columns = ['*'])
 * @method Unidad first($columns = ['*'])
*/
class UnidadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'abreviatura',
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Unidad::class;
    }
}
