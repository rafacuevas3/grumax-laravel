<?php

namespace App\Repositories;

use App\Models\Clasificacion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ClasificacionRepository
 * @package App\Repositories
 * @version November 13, 2017, 10:32 pm UTC
 *
 * @method Clasificacion findWithoutFail($id, $columns = ['*'])
 * @method Clasificacion find($id, $columns = ['*'])
 * @method Clasificacion first($columns = ['*'])
*/
class ClasificacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clasificacion::class;
    }
}
