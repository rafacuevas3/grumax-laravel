<?php

namespace App\Repositories;

use App\Models\Almacen;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AlmacenRepository
 * @package App\Repositories
 * @version November 13, 2017, 6:54 pm UTC
 *
 * @method Almacen findWithoutFail($id, $columns = ['*'])
 * @method Almacen find($id, $columns = ['*'])
 * @method Almacen first($columns = ['*'])
*/
class AlmacenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Almacen::class;
    }
}
