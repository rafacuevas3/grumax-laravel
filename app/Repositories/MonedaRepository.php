<?php

namespace App\Repositories;

use App\Models\Moneda;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MonedaRepository
 * @package App\Repositories
 * @version November 13, 2017, 5:51 pm UTC
 *
 * @method Moneda findWithoutFail($id, $columns = ['*'])
 * @method Moneda find($id, $columns = ['*'])
 * @method Moneda first($columns = ['*'])
*/
class MonedaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Moneda::class;
    }
}
