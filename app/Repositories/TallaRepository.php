<?php

namespace App\Repositories;

use App\Models\Talla;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TallaRepository
 * @package App\Repositories
 * @version November 13, 2017, 10:36 pm UTC
 *
 * @method Talla findWithoutFail($id, $columns = ['*'])
 * @method Talla find($id, $columns = ['*'])
 * @method Talla first($columns = ['*'])
*/
class TallaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Talla::class;
    }
}
