<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\hasContacts;
use App\Http\Traits\hasLocations;

class Cliente extends Model
{
    use SoftDeletes;
    
    use hasContacts;
    use hasLocations;

    protected $fillable = [
        'nombre',
        'nombre_oficial',
        'nombre_factura',
        'comentario',
        'logo',
        'rfc',
        'tipo',
        'isEmbarque'
    ];
}
