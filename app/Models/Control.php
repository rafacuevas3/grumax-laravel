<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control extends Model
{
    use SoftDeletes;

    protected $table = 'controllers';

    protected $fillable = ['nombre'];

    public static LECTURA = 1;
    public static ESCRITURA = 2;
    public static CONFIGURACION = 3;
    public static ADMIN = 4;

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_controllers', 'id_controller', 'id_user');
    }
}
