<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $fillable = [
        'nombre',
        'departamento',
        'puesto',
    ];

    public $timestamps = false;
    
    public function contactable()
    {
        return $this->morphTo();
    }

    public function telefonos()
    {
        return $this->hasMany('App\Models\Telefono', 'id_contacto');
    }

    public function correos()
    {
        return $this->hasMany('App\Models\Correo', 'id_contacto');
    }

    // Create Methods

    public function addTelefono($data)
    {
        return $this->telefonos()->save(new Telefono($data));
    }

    public function addCorreo($data)
    {
        return $this->telefonos()->save(new Correo($data));
    }

    // Update Methods

    public function updateTelefono($id, $data)
    {
        return $this->telefonos()->updateOrCreate(['id' => $id], $data);
    }

    public function updateCorreo($id, $data)
    {
        return $this->correos()->updateOrCreate(['id' => $id], $data);
    }

    // Delete Methods

    public function deleteTelefonos($old)
    {
        return \DB::table('telefonos')
                  ->where('id_contacto', $this->id)
                  ->whereNotIn('id', $old)
                  ->delete();
    }

    public function deleteCorreos($old)
    {
        return \DB::table('correos')
                  ->where('id_contacto', $this->id)
                  ->whereNotIn('id', $old)
                  ->delete();
    }
}
