<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\hasContacts;
use App\Http\Traits\hasLocations;

class Proveedor extends Model
{
    use SoftDeletes;
    
    use hasContacts;
    use hasLocations;

    protected $table = "proveedores";

    protected $fillable = [
        'id_moneda',
        'nombre',
        'nombre_oficial',
        'nombre_factura',
        'comentario',
        'rfc',
        'tipo',
        'iva',
        'isTransporte',
        'isEmbarque'
    ];

    public function moneda()
    {
        return $this->belongsTo('App\Models\Moneda', 'id_moneda');
    }
}
