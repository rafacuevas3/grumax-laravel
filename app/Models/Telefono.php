<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    public $timestamps = false;

    protected $table = 'telefonos';

    protected $fillable = [
        'id_contacto',
        'telefono',
        'tipo'
    ];
}
