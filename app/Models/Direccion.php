<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    const FACTURA = 0;
    const RECOLECCION = 1;

    protected $table = 'direcciones';

    public $timestamps = false;

    protected $fillable = [
        'tipo_direccion',
        'nombre',
        'comentario',
        'calle',
        'numero',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'telefono',
    ];

    public function locations()
    {
        return $this->morphTo();
    }
}
