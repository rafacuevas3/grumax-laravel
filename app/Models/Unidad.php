<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unidad
 * @package App\Models
 * @version November 10, 2017, 12:41 am UTC
 *
 * @property string abreviatura
 * @property string nombre
 */
class Unidad extends Model
{
    use SoftDeletes;

    public $table = 'unidades';    

    protected $dates = ['deleted_at'];

    public $fillable = [
        'abreviatura',
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'abreviatura' => 'string',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'abreviatura' => 'required',
        'nombre' => 'required'
    ];

    
}
