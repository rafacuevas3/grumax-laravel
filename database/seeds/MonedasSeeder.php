<?php

use Illuminate\Database\Seeder;

class MonedasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('monedas')->insert(['nombre' => "Peso Mexicano"]);
         DB::table('monedas')->insert(['nombre' => "Dolar Americano"]);
    }
}
