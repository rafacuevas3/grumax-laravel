<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_moneda')->unsigned();

            $table->string('nombre', 150);
            $table->string('nombre_oficial', 150);
            $table->string('nombre_factura', 150);
            $table->string('comentario');
            $table->string('rfc', 50);
            $table->enum('tipo', ['Nacional', 'Internacional']);
            $table->tinyInteger('iva');
            $table->tinyInteger('num_compras')->default(0);
            $table->tinyInteger('isTransporte')->default(0);
            $table->tinyInteger('isEmbarque')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_moneda')->references('id')->on('monedas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
