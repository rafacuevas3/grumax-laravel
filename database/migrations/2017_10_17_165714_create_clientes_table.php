<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre', 150);
            $table->string('nombre_oficial', 150);
            $table->string('nombre_factura', 150);
            $table->string('comentario');
            $table->string('rfc', 50);
            $table->string('logo', 255);
            $table->enum('tipo', ['Nacional', 'Internacional']);
            $table->tinyInteger('isEmbarque');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
