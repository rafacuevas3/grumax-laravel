<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('locations_id')->unsigned()->index();
            $table->string('locations_type', 255)->index();
            
            $table->string('tipo_direccion');
            
            $table->string('nombre', 80);
            $table->string('comentario', 150);
            $table->string('calle', 45);
            $table->string('numero', 10);
            $table->string('colonia', 45);
            $table->string('municipio', 45);
            $table->string('estado', 45);
            $table->string('pais', 45);
            $table->string('cp', 45);
            $table->string('telefono', 25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
